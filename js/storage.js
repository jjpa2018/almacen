function registro(base,tipo)
{
	var	almacen=$("#almacen").val();
	var	pruebaIdSA=$("#pruebaIdSA").val();
	var	base=base;
	var tipo=tipo;
	var ruta="index.php/storage/main/";
	$.ajax({
		type:'POST',
		url:base+ruta+tipo,
		data:{'almacen':almacen,'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#view_form").html(data); 
		}
	})
} 

function modal(base,tipo)
{
	var	almacen = $("#almacen").val();
	var	nombre = $("#nombre").val(); 
	var	tipo = tipo;
	var base = base;
	$.ajax({
		type:'POST', 
		url: base+'index.php/storage/loadView/'+tipo,
		data:{'almacen':almacen,'nombre':nombre},
		cache:false,
		success:function(data)
		{
			$("#loadView").html(data);
		}
	})
}

function confi(base,tipo)
{
	var	almacen=$("#almacen").val();
	var	pruebaIdSA=$("#pruebaIdSA").val();
	var	base=base;
	var tipo=tipo;
	var ruta="index.php/storage/confi/";
	$.ajax({
		type:'POST',
		url:base+ruta+tipo,
		data:{'almacen':almacen,'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data) 
		{
			$("#view_confi").html(data);
		}
	})
} 

function almacenes(base,idu)
{
	var idu = idu;
	var base = base+'index.php/inicio/loadStorage/';
	$.ajax({
		type:'POST',
		url: base+idu,
		data:{'idu':idu},
		cache:false,
		success:function(data)
		{
			$("#almacenes").html(data);
		}
	})	
}

function loadSubStorage(base,idalmacen)
{
 
	var almacen = idalmacen;
	var base = base+'index.php/inicio/loadSubStorage';
	$.ajax({
		type:'POST',
		url: base,
		data:{'almacen':almacen},
		cache:false,
		success:function(data)
		{
			$("#subalmacenes").html(data);
		}
	})	
}

function guardar(base)
{
	var nombre_almacen_new = $("#nombre_almacen_new").val();
	var ruta = base+'index.php/storage/newstorage/';
	var	idu=$("#imagen").val();
	$.ajax({
		type:'POST',
		url: ruta+idu,
		data:{
			'nombre_almacen_new':nombre_almacen_new
		},
		cache:false,
		success:function(data)
		{
			if (data == 1) 
			{
				alert("Creado con exito");
				$('#subAlmacen1').modal('hide');
				almacenes(base,idu);
			}
			else
			{
				alert("Error en la creación del almacen");
				$('#subAlmacen1').modal('hide');
			}
		}
	})
}

function guardarSubaLmacen(base,idu)
{
	var nombre_subalmacen = $("#nombre_subalmacen").val();
	var id_almacen_new = $("#id_almacen_new").val();
	var	idu=$("#imagen").val();
	var ruta = base+'index.php/storage/Subalmacen/';
	$.ajax({
		type:'POST',
		url: ruta+idu,
		data:{
			'nombre_subalmacen':nombre_subalmacen,
			'id_almacen_new':id_almacen_new
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			if (data != 0) 
			{
				alert("Creado con exito");
				var val = $.parseJSON(data);
				$("#pruebaIdSA").val(val);
				registro(base,'2');
				loadSubStorage(base,idu);
			}
			else
			{
				alert("Error en la creación del almacen");
				$('#subAlmacen1').modal('hide');
			}
		}
	})
}

function guardarSubaLmacenU(base)
{
	var nombre_subalmacen = $("#nombre_subalmacen").val();
	var id_almacen_new = $("#id_almacen_new").val();
	var	id_sa=$("#id_sa").val();
	var ruta = base+'index.php/storage/SubalmacenU';
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'nombre_subalmacen':nombre_subalmacen,
			'id_almacen_new':id_almacen_new,
			'id_sa':id_sa
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			if (data != 0) 
			{
				alert("Registro Actualizado");
				registro(base,'2');
			}
			else
			{
				alert("Error en la Actualización del registro");
			}
		}
	})
}

function habilita_texto()
{
	pruebaIdSA=$("#pruebaIdSA").val();
	if (pruebaIdSA != '') 
	{
		$("#info").removeClass('none');
	}
	else
	{
		alert("Para cargar plano, debe primero registrar un sub alamacen");
	}
	
}

function ok()
{
	$("#info").addClass('none');
	$("#post-info").addClass('none');
	$("#loadImage").removeClass('none');
}

function insertCuadrante(base,tipo)
{
	var pruebaIdSA = $("#pruebaIdSA").val();
	var cantCuadrante = $("#cantCuadrante").val();
	if (tipo == 0 || tipo == 1) 
	{
		var	ruta = base+'index.php/storage/insertCuadrante';
	}
	else
	{
		var	ruta = base+'index.php/storage/insertCuadranteU';
	}
	
	
	var result = confirm("¿Esta seguro de crear "+cantCuadrante+" cuadrante(es)?");
	if (result) 
	{
		$.ajax({
			type:'POST',
			url: ruta,
			data:{
				'pruebaIdSA':pruebaIdSA, 
				'cantCuadrante':cantCuadrante
			},
			cache:false,
			success:function(data)
			{
				if (data == 0) 
				{
					alert("Para configurar Cuadrantes, debe primero registrar un sub alamacen");
				}
				else
				{
					$("#cCuadrante").hide();
					$("#confiPaleta").html(data);				
					$("#confPaleta").hide();
					$("#confPaletaU").show();
					
				}
			}
		})
	}
}

function guardarConfiPaleta(base,cantCuadrante,tipo)
{
	var ruta = base+'index.php/storage/IConfiPaleta';
	for (var i = 0; i < cantCuadrante; i++) {
		var id_cuadrante = [$("#id_cuadrante"+i).val()];
		var paleta = [$("#paleta"+i).val()];
		$.ajax({
			type:'POST',
			url: ruta,
			data:{
				'id_cuadrante':id_cuadrante,
				'paleta':paleta
			},
			cache:false,
			datatype:'json',
			success:function(data)
			{
				if (tipo == 1 || tipo == 2) 
				{
					registro(base,3);
				}
			}
		})
	}
	alert("Creado con exito");
	confi(base,2);
}

function insertEstante(base,tipo)
{
	var pruebaIdSA = $("#pruebaIdSA").val();
	var cantEstante = $("#cantEstante").val();

	if (tipo == 0 || tipo == 1) 
	{
		var	ruta = base+'index.php/storage/insertEstante';
	}
	else
	{
		var	ruta = base+'index.php/storage/insertEstanteU';
	}
	
	var result = confirm("¿Esta seguro de crear "+cantEstante+" estante(es)?");
	if (result) 
	{
		$.ajax({
			type:'POST',
			url: ruta,
			data:{
				'pruebaIdSA':pruebaIdSA,
				'cantEstante':cantEstante
			},
			cache:false,
			success:function(data)
			{
				if (data == 0) 
				{
					alert("Para configurar Estantes, debe primero registrar un sub alamacen");
				}
				else
				{
					$("#cEstante").hide();
					$("#confiEntrepano").html(data);				
					$("#confEntrepano").hide();
					$("#confEntrepanoU").show();
				}
			}
		})
	}
}

function guardarConfiEntrepano(base,cantEstante,tipo)
{
	var ruta = base+'index.php/storage/IConfiEntrepano';
	for (var i = 0; i < cantEstante; i++) {
		var id_estante = [$("#id_estante"+i).val()];
		var entrepano = [$("#entrepano"+i).val()];
		$.ajax({
			type:'POST',
			url: ruta,
			data:{
				'id_estante':id_estante,
				'entrepano':entrepano
			},
			cache:false,
			datatype:'json',
			success:function(data)
			{
				if (tipo == 1 || tipo == 2) 
				{
					registro(base,3);
				}
			}
		})
	}
	alert("Creado con exito");
	confi(base,3);
	
}

function insertRack(base)
{
	var pruebaIdSA = $("#pruebaIdSA").val();
	var name_rack = $("#name_rack").val();
	var	ruta = base+'index.php/storage/insertRack';
	var result = confirm("¿Está seguro de crear el rack: "+name_rack+"?");
	if (result) 
	{
		$.ajax({
			type:'POST',
			url: ruta,
			data:{
				'pruebaIdSA':pruebaIdSA,
				'name_rack':name_rack
			},
			cache:false,
			success:function(data)
			{
				if (data == 0) 
				{
					alert("Para configurar Racks, debe primero registrar un sub alamacen");
				}
				else
				{
					$("#bRack").hide();
					$("#confiRack").html(data);

				}
			}
		})
	}
}

function guardarConfiRac(base)
{
	var ruta = base+'index.php/storage/IConfiRack';
	var id_rack = $("#id_rack").val();
	var fRack = $("#fRack").val();
	var cRack = $("#cRack").val();
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id_rack':id_rack,
			'fRack':fRack,
			'cRack':cRack
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			if (data != 0) 
			{
				alert("Creado con exito");
				$("#infoRack").html(data);
				$("#name_rack").val('');
				$("#fRack").val('');
				$("#cRack").val('');
				$("#bRack").show();

			}
			else
			{
				//alert("Error en la creación del almacen");
				//$('#subAlmacen1').modal('hide');
			}
		}
	})
}

function confiCertificacion(base,tipo)
{
	var	almacen=$("#almacen").val();
	var	pruebaIdSA=$("#pruebaIdSA").val();
	var	base=base;
	var tipo=4;
	var ruta="index.php/storage/confi/";
	$.ajax({
		type:'POST',
		url:base+ruta+tipo,
		data:{'almacen':almacen,'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#view_confi").html(data);
		}
	})
}

function delet(base,tipo,idcer,id) 
{
	var	pruebaIdSA=$("#pruebaIdSA").val();
	if (tipo == 1) 
	{
		var	ruta=base+'index.php/storage/cuadranteE';
	}
	if (tipo==2)
	{
		var	ruta=base+'index.php/storage/estanteE';
	}
	if (tipo==3) 
	{
		var	ruta=base+'index.php/storage/rackE';
	}
	if (tipo==99) 
	{
		var	ruta=base+'index.php/storage/cuadranteSE';
	}
	if (tipo==98) 
	{
		var	ruta=base+'index.php/storage/estanteSE';
	}
	var result = confirm("¿Está seguro de querer eliminar este registro?");
	if (result) 
	{
		$.ajax({
			type:'POST',
			url:ruta,
			data:{'id':id,'pruebaIdSA':pruebaIdSA,'idcer':idcer},
			cache:false,
			success:function(data)
			{
				if (data == 1) 
				{
					alert("Registro Eliminado con exito");
					registro(base,3);
				}
				else
				{
					alert("Error al intentar eliminar el registro, posee mercancia en el almacen");
				}
			}
		})
	}
}


function siguienteConfirm(base)
{
	registro(base,4);
}

function search(base,idSubalmacen,caso)
{
		var	pruebaIdSA = idSubalmacen;
	
	if (caso == 1) 
	{
		var ruta = base+'index.php/storage/search';
	}
	else
	{
		var ruta = base+'index.php/storage/searchStorage';
	}
	
	if (caso == 1) 
	{
		var	almacen=$("#almacen").val();
		var datos = {'pruebaIdSA':pruebaIdSA,'almacen':almacen};
	}
	else
	{
		var datos = {'pruebaIdSA':pruebaIdSA};
	}


	$.ajax({
		type:'POST',
		url:ruta,
		data:datos,
		cache:false,
		datatype:'json',
		success:function(data)
		{
			$("#loadView").html(data);
		}
	});
}

function edit(base,idSubalmacen)
{
	var ruta = base+'index.php/storage/edit';
	var	pruebaIdSA = idSubalmacen;
	var	almacen=$("#almacen").val();

	$.ajax({
		type:'POST',
		url:ruta,
		data:{'pruebaIdSA':pruebaIdSA,'almacen':almacen},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			$("#loadView").html(data);
		}
	});
}

function create_cuadrante(base,tipo)
{
	var	ruta= base+'index.php/storage/viewEdit/';
	var	pruebaIdSA=$("#pruebaIdSA").val();

	$.ajax({
		type:'POST',
		url:ruta+tipo,
		data:{'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#create_cuadrante").html(data);
			$("#msj_err_c").hide(); 
			$("#tittle_c").hide();
			$("#save_c").hide();
			$("#save_cU").show();
			
		}
	});
}

function create_paleta(base,tipo)
{
	var	ruta= base+'index.php/storage/viewEdit/';
	var	pruebaIdSA=$("#pruebaIdSA").val();

	$.ajax({
		type:'POST',
		url:ruta+tipo,
		data:{'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#create_cuadrante").html(data);			
		}
	});
}

function create_estante(base,tipo)
{
	var	ruta= base+'index.php/storage/viewEdit/';
	var	pruebaIdSA=$("#pruebaIdSA").val();

	$.ajax({
		type:'POST',
		url:ruta+tipo,
		data:{'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#create_estante").html(data);
			$("#msj_err_e").hide();
			$("#tittle_e").hide();
			$("#save_e").hide();
			$("#save_eU").show();
		}
	});
}

function create_entrepano(base,tipo)
{
	var	ruta= base+'index.php/storage/viewEdit/';
	var	pruebaIdSA=$("#pruebaIdSA").val();

	$.ajax({
		type:'POST',
		url:ruta+tipo,
		data:{'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#create_estante").html(data);
		}
	});
}

function create_rack(base,tipo)
{
	var	ruta= base+'index.php/storage/viewEdit/';
	var	pruebaIdSA=$("#pruebaIdSA").val();

	$.ajax({
		type:'POST',
		url:ruta+tipo,
		data:{'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#create_rack").html(data);
			$("#msj_err_r").hide();
			$("#tittle_r").hide();
			$("#save_r").hide();
			$("#save_rU").show(); 
		}
	});
}

function create_fila(base,tipo)
{
	var	ruta= base+'index.php/storage/viewEdit/';
	var	pruebaIdSA=$("#pruebaIdSA").val();

	$.ajax({
		type:'POST',
		url:ruta+tipo,
		data:{'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#create_rack").html(data);
		}
	});
}

function create_columna(base,tipo)
{
	var	ruta= base+'index.php/storage/viewEdit/';
	var	pruebaIdSA=$("#pruebaIdSA").val();

	$.ajax({
		type:'POST',
		url:ruta+tipo,
		data:{'pruebaIdSA':pruebaIdSA},
		cache:false,
		success:function(data)
		{
			$("#create_rack").html(data);
		}
	});
}

function guardarConfiPaletaU(base,cantCuadrante,tipo)
{
	var ruta = base+'index.php/storage/IConfiPaleta';
	var	cont=$("#cont").val();
	for (var i = cont; i < cantCuadrante; i++) {
		var k=parseInt(i-cont);
		var id_cuadrante = [$("#id_cuadrante"+k).val()];
		var paleta = [$("#paleta"+k).val()];
		$.ajax({
			type:'POST',
			url: ruta,
			data:{
				'id_cuadrante':id_cuadrante,
				'paleta':paleta
			},
			cache:false,
			datatype:'json', 
			success:function(data)
			{
				registro(base,3);
			}
		})
	}
	alert("Creado con exito");	
}

function eleccion(base)
{
	var	id = $("#id").val();
	var ruta = base+'index.php/storage/detalles';
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id':id,
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			var val = $.parseJSON(data);
			$("#nombre_paleta").text(val[0]);

		}
	})
}

function load_name(base)
{
	var	id = $("#id").val();
	var ruta = base+'index.php/storage/detallesPaleta';
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id':id,
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			var val = $.parseJSON(data);
			$("#paleta").html(val);

		}
	})
}

function guardarConfiPaletaUII(base,tipo)
{
	var ruta = base+'index.php/storage/IConfiPaletaU';
	var id = $("#id").val();
	var paleta = $("#paleta").val();
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id':id,
			'paleta':paleta
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			alert("Creado con exito");
			registro(base,3);
		}
	})
}

function guardarConfiEntrepanoU(base,cantEstante,tipo)
{
	var ruta = base+'index.php/storage/IConfiEntrepano';
	var	cont=$("#cont").val();
	for (var i = cont; i < cantEstante; i++) {
		var k=parseInt(i-cont);
		var id_estante = [$("#id_estante"+k).val()];
		var entrepano = [$("#entrepano"+k).val()];
		$.ajax({
			type:'POST',
			url: ruta,
			data:{
				'id_estante':id_estante,
				'entrepano':entrepano
			},
			cache:false,
			datatype:'json', 
			success:function(data)
			{
				registro(base,3);
			}
		})
	}
	alert("Creado con exito");	
}

function guardarConfiEntrepanoUII(base,tipo)
{
	var ruta = base+'index.php/storage/IConfiEntrepanoU';
	var id = $("#id").val();
	var entrepano = $("#entrepano").val();
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id':id,
			'entrepano':entrepano
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			alert("Creado con exito");
			registro(base,3);
		}
	})
}

function eleccionE(base)
{
	var	id = $("#id").val();
	var ruta = base+'index.php/storage/detallesE';
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id':id,
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			var val = $.parseJSON(data);
			$("#nombre_entrepano").text(val[0]);

		}
	})
}

function load_nameE(base)
{
	var	id = $("#id").val();
	var ruta = base+'index.php/storage/detallesEntrepanoE';
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id':id,
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			var val = $.parseJSON(data);
			$("#entrepano").html(val);

		}
	})
}

function insertRackU(base)
{
	var pruebaIdSA = $("#pruebaIdSA").val();
	var name_rack = $("#name_rack").val();
	var	ruta = base+'index.php/storage/insertRackU';
	var result = confirm("¿Está seguro de crear el rack: "+name_rack+"?");
	if (result) 
	{
		$.ajax({
			type:'POST',
			url: ruta,
			data:{
				'pruebaIdSA':pruebaIdSA,
				'name_rack':name_rack
			},
			cache:false,
			success:function(data)
			{
				if (data == 0) 
				{
					alert("Para configurar Racks, debe primero registrar un sub alamacen");
				}
				else
				{
					$("#bRack").hide();
					$("#confiRack").html(data);

				}
			}
		})
	}
}

function load_nameR(base)
{	
	var	id_rack = $("#id_rack").val();
	var ruta = base+'index.php/storage/detallesRowR';
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id_rack':id_rack,
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			var val = $.parseJSON(data);
			$("#pru").html(val);

		}
	})
}

function guardarConfiRacU(base)
{
	var ruta = base+'index.php/storage/IConfiRack';
	var id_rack = $("#id_rack").val();
	var fRack = $("#fRack").val();
	var cRack = $("#cRack").val();
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id_rack':id_rack,
			'fRack':fRack,
			'cRack':cRack
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			if (data != 0) 
			{
				alert("Creado con exito");
				registro(base,3);
			}
			else
			{
				//alert("Error en la creación del almacen");
				//$('#subAlmacen1').modal('hide');
			}
		}
	})
}

function guardarConfiRacUI(base)
{
	var ruta = base+'index.php/storage/IConfiRackU';
	var id_rack = $("#id_rack").val();
	var fRack = $("#fRack").val();
	var cRack = $("#cRack").val();
	var fRackh = $("#fRackh").val();
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id_rack':id_rack,
			'fRack':fRack,
			'cRack':cRack,
			'fRackh':fRackh
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			if (data != 0) 
			{
				alert("Creado con exito");
				registro(base,3);
			}
			else
			{
				//alert("Error en la creación del almacen");
				//$('#subAlmacen1').modal('hide');
			}
		}
	})
}

function load_nameCR(base)
{
	var	id_rack = $("#id_rack").val();
	var ruta = base+'index.php/storage/detallesColumnR';
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id_rack':id_rack
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			var val = $.parseJSON(data);
			$("#id_fila").html(val);

		}
	})
}

function guardarConfiRacUII(base)
{
	var ruta = base+'index.php/storage/IConfiRackUI';
	var id_rack = $("#id_rack").val();
	var id_fila = $("#id_fila").val();
	var cRack = $("#cRack").val();
	var cRackh = $("#cRackh").val();
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id_rack':id_rack,
			'id_fila':id_fila,
			'cRack':cRack,
			'cRackh':cRackh
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			if (data != 0) 
			{
				alert("Creado con exito");
				registro(base,3);
			}
			else
			{
				//alert("Error en la creación del almacen");
				//$('#subAlmacen1').modal('hide');
			}
		}
	})
}

function deletes(base,id)
{
	var	ruta=base+'index.php/storage/subalmacenE';
	var	idu=$("#imagen").val();
	var result = confirm("¿Está seguro de querer eliminar este subalmacen?");
	if (result) 
	{
		$.ajax({
			type:'POST',
			url:ruta,
			data:{'id':id},
			cache:false,
			success:function(data)
			{
				if (data == 1) 
				{

					alert("Registro Eliminado con exito");
					loadSubStorage(base,idu);
					$('#subAlmacen1').modal('hide');	
					$('.modal-backdrop').remove();
					location.href();
				}
				else
				{
					alert("Error al intentar eliminar el registro, posee mercancia en el almacen");
					$('#subAlmacen1').modal('hide');
				}
			}
		})
	}
	else
	{
		loadSubStorage(base,idu);
		$('#subAlmacen1').modal('hide');
		$('.modal-backdrop').remove();
		location.href();
	}
}

function loadColumn(base)
{
	var	id_rack = $("#id_rack").val();
	var	id_fila=$("#id_fila").val();
	var ruta = base+'index.php/storage/detalleColumn';
	$.ajax({
		type:'POST',
		url: ruta,
		data:{
			'id_rack':id_rack,
			'id_fila':id_fila
		},
		cache:false,
		datatype:'json',
		success:function(data)
		{
			var val = $.parseJSON(data);
			$("#pruC").html(val);

		}
	})
}