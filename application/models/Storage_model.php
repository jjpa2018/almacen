<?php 

class Storage_model extends CI_Model
{

	/**
	 * la funcion genera_numero es usada para generar uncodigo aleatorio que sera usada como codigo en los diferentes niveles de almacenamiento, esta funcion genera un numero unico.
	 */
	public function genera_numero()
	{
		$codigo=rand(1,9999);//rand que genera el numero aleatorio
		$data=$this->no_duplic_num_solicitudC($codigo);// aqui se hace referencia al metodo no_duplic_num_solicitudC pasando el rand que se fue creado.
		return $data;
	}

	/**
	 * la funcion no_duplic_num_solicitudC se encarga de validar que el codigo que se genero anteriormente no este duplicado en la base de datos
	 */
	public function no_duplic_num_solicitudC($codigo)
	{
		$data=$this->no_duplic_num_solicitud($codigo);//aqui se hace referencia al metodo no_duplic_num_solicitud pasando el codigo recibido por parametro.
		if ($data == 1) //pregunto si el resultado del llamado que se hizo anteriormente es igual a 1
		{// si es igual a uno vuelve a generar un codigo aleatorio nuevamente
			$duplicado=$this->genera_numero();
		}
		else
		{ //si no retorna el codigo
			return $codigo;
		}
	}


	/**
	 * la funcion no_duplic_num_solicitud es la funcion que se encarga de verificar con la base de datos que el codigo que fue generado en la funcion genera_numero se encuentre registrada
	 */
	public function no_duplic_num_solicitud($codigo)
	{
		$registro="SELECT codigo FROM unidad_negocio where codigo = '$codigo'";
		$query=$this->db->query($registro);
		$numero=$query->num_rows();// cuento las filas que retorna la consulta.
		if ($numero > 0) // aqui verifica si el resultado de la consulta trae mas de un registro
		{// si el resultado es mayor a 0 entonces retorno 1
			return 1;
		}
		else
		{ // si no retorno 0;
			return 0;
		}
	}

	/**
	 * la funcion select es la encargada de consultar los datos de ubicacion del almacen en el que se encuentra el usuario, o tiene asignado el usuario para mostrarlo en la pantalla principal
	 */
	public function select($idu)
	{
		$query = "SELECT unidad_negocio.id as idalmacen,
		hospital, 
		estado_id, 
		tipo_id, 
		region_id, 
		ref.referencia as tipo_localidad, 
		ref1.referencia as region, 
		direccion.nombre as estado, 
		establecimientos.id as sedeId 
		FROM unidad_negocio
		inner join usuario_has_unidad_negocio on unidad_negocio.id = unidad_negocio_id
		inner join establecimientos on extras_id = establecimientos.id
		inner join referencia as ref on ref.id = tipo_id
		inner join referencia as ref1 on ref1.id = region_id
		inner join direccion on estado_id = direccion.id
		where usuario_id = $idu and unidad_negocio_id != 515";
		$result= $this->db->query($query);

		$cantidad=$result->num_rows(); // cuento la cantidad de filas que retorna la consulta
		if ($cantidad > 0) // verifico que sea mayor a 0 
		{ // si es mayor a 0 que recorra el registro y retorna a $data
			foreach ($result->result() as $option) {
				$data[] = array( $option->idalmacen, 
					$option->idalmacen,
					$option->hospital,
					$option->estado_id,
					$option->tipo_id,
					$option->region_id,
					$option->tipo_localidad,
					$option->region,
					$option->estado,
					$option->sedeId
				);
			}
			return $data;
		}
		else 
		{// si no que me retorne un valor donde defino que o trajo ningun registro
			return 1000;
		}
	}


	/**
	 * la funcion almacen retorna los almacenes que tiene asignado el usuario que esta logueado
	 */
	public function almacen($idu)
	{
		$query="SELECT un.id as idalmacen,
		un.nombre as name
		FROM unidad_negocio as un
		inner join usuario_has_unidad_negocio as unu on unu.unidad_negocio_id = un.id
		inner join establecimientos as est on un.extras_id = est.id
		where unu.usuario_id = $idu and un.id != 515";
		$almacen=$this->db->query($query);
		$cantidad=$almacen->num_rows();// cuento la cantidad de filas que retorna la consulta
		if ($cantidad>0) // verifico que sea mayor a 0 
		{// si es mayor a 0 que recorra el registro y retorna a $data
			foreach ($almacen->result() as $option)  
			{
				$data[] = array(	
								$option->idalmacen,
								$option->name
							   );
			}
			return $data;
		}
		else
		{ // si no que me retorne un valor donde defino que o trajo ningun registro
			return 1000;
		}
	}

	/**
	 * la funcion loadSubStorage o cargar subalmacen, recibe como parametro el id del almacen que alla elegido el usuario y consulta los subalmacenes pertenecientes al almacen que eligio el usuario
	 */
	public function loadSubStorage($almacen)
	{
		$query="SELECT id, nombre 
		FROM unidad_negocio 
		where empresa_id = '$almacen'";
		$subalmacen=$this->db->query($query);

		$cantidad=$subalmacen->num_rows();
		if ($cantidad>0) // verifico que sea mayor a 0  
		{// si es mayor a 0 que recorra el registro y retorna a $data
			foreach ($subalmacen->result() as $option) 
			{
				$data[]=array($option->id,
					$option->nombre);
			}
			return $data;
		}
		else
		{// si no que me retorne un valor donde defino que o trajo ningun registro
			return 1000;
		}
	}

	/**
	 * la funcion infoStorage me consulta y devuelve la informacion del almacen (id y nombre) al que pertenece el subalmacen 
	 */
	function infoStorage($almacen)
	{
		$query="SELECT
		id as id_almacen,
		nombre as nombre_almacen
		FROM unidad_negocio 
		WHERE id = '$almacen'";
		$info=$this->db->query($query);
		$cantidad=$info->num_rows();
		if ($cantidad>0) // verifico que sea mayor a 0  
		{// si es mayor a 0 que recorra el registro y retorna a $data
			foreach ($info->result() as $option) 
			{
				$data[]=array($option->id_almacen,
					$option->nombre_almacen);
			}
			return $data;
		}
		else
		{// si no que me retorne un valor donde defino que o trajo ningun registro
			return 1000;
		}

	}

	/**
	 * la funcion newStorage se encarga de gestionar la insercion de un almacen creado por el usuario y la insercion en la tabla de usuario_has_unidad_negocio
	 */
	public function newStorage($nameStorage,$idu)
	{
		$codeStorage= $this->genera_numero(); //crea aqui el codigo aleatorio.
		$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, correo, ubcn, empresa_id, pais_id, extras_id)
		VALUES ('$codeStorage','$nameStorage',0,22,'ivss@gmail.com',0,'515','196','53')";
		$insertNewStorage=$this->db->query($query); // realizo la insercion en la tabla de unidad_negocio con los datos pasado por paramtros
		$query="SELECT max(id) as id FROM unidad_negocio where codigo = '$codeStorage'";
		$select=$this->db->query($query); // selecciono el id del almacen creado.
		foreach ($select->result() as $key)
		{
			$query="INSERT INTO usuario_has_unidad_negocio (usuario_id,unidad_negocio_id)
			VALUES ('$idu','$key->id')";
			$insertUhun=$this->db->query($query); // realizo la insercion en la tabla de usuario_has_unidad_negocio
		}
		
		if ($insertNewStorage = true and $insertUhun = true)  // verifico que loas 2 inserciones se hayan hecho sin ningun contratiempo
		{// si las inserciones se hicieron de manera exito se elimina el codigo aleatorio creado
			$query="UPDATE unidad_negocio SET codigo  = NULL where codigo = '$codeStorage' ";
			$deleteCode=$this->db->query($query);
			return 1;
		}
		else
		{// en el caso de que haya ocurrido algun error en alguna de las inserciones 
			$query="SELECT max(id) as id FROM unidad_negocio where codigo = '$codeStorage'";
			$select=$this->db->query($query);// selecciono el id del almacen creado
			if ($select->num_rows()>0) //verifico que de la consulta realizada retorne un valor mayor a 0
			{// si existe el registro de un almacen elimino los registros 
				foreach ($select->result() as $key)
				{
					$query="DELETE FROM unidad_negocio where id = '$key->id'";
					$delete=$this->db->query($query);	
					$query="DELETE FROM usuario_has_unidad_negocio where unidad_negocio_id = '$key->id'";
					$deleteUhun=$this->db->query($query);
				}
				return 1000;
			}
			else
			{
				return 1000;
			}
			
		}
	}

	/**
	 * la funcion Subalmacen gestiona la insercion del nombre del subalmacen recibe como parametros el id del alamcen al cual va a pertenecer, el id del usuario que lo creo
	 */
	public function Subalmacen($nameStorage,$idu,$almacen)
	{
		$codeStorage= $this->genera_numero(); // se crea aqui el codigo aleatorio.
		$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, correo, ubcn, empresa_id, pais_id, extras_id)
		VALUES ('$codeStorage','$nameStorage',0,22,'ivss@gmail.com',1,'$almacen','196','53')";
		$insertNewStorage=$this->db->query($query); // realizo la insercion en la tabla de unidad_negocio con los datos pasado por parametros
		
		if ($insertNewStorage = true) // verifico que se haya realizado la insercion
		{// si se realizo la insercion limpio el campo de codigo.
			$query="SELECT id as id FROM unidad_negocio where codigo = '$codeStorage'";
			$select=$this->db->query($query);
			$query="UPDATE unidad_negocio SET codigo  = NULL where codigo = '$codeStorage' ";
			$deleteCode=$this->db->query($query);
			if ($select->num_rows()>0) {
				foreach ($select->result() as $option)
				{
					$data[]=array($option->id);
				}
				return $data; // envio al controlador el id del subalmacen creado
			}
			else
			{
				return 0;
			}
		}
		else
		{// si hubo algun error en la insercion
			$query="SELECT max(id) as id FROM unidad_negocio where codigo = '$codeStorage'";
			$select=$this->db->query($query); // selecciono el id del subalmacen.
			if ($select->num_rows()>0) // pregunto si retorna algun registro
			{// si tiene algun registro lo elimina
				foreach ($select->result() as $key)
				{
					$query="DELETE FROM unidad_negocio where id = '$key->id'";
					$delete=$this->db->query($query);
				}
				return 0;
			}
			else
			{// si no retorna 0
				return 0;
			}
			
		}
	}

	/**
	 * la funcion insertCuadrante gestiona la insercion de los cuadrantes en el subalmacen que se esta creando
	 */
	public function insertCuadrante($almacen,$cantCuadrante)
	{
		if ($almacen != '') // verifico que al momento de intentar realizar la insercion haya un subalmacen creado para poder realizar la insercion
		{// si existe un subalmacen esta creado
			$codeStorage= $this->genera_numero(); // genero un codigo
			for ($i=1; $i <= $cantCuadrante ; $i++) // verifico la cantidad de cuadrantes que el usuario eligio para crear
			{ 
				$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id)
				VALUES ('$codeStorage','$i',0,22,2,'$almacen','196','53')";
				$insertNewStorage=$this->db->query($query); // realiza x´s cantidad de inserciones segun los cuadrantes definidos por el usuario
			}
			$query="SELECT id FROM unidad_negocio where codigo = '$codeStorage' and empresa_id = $almacen";
			$selectId=$this->db->query($query); // extraigo el id de los cuadrantes
			$cantidad=$selectId->num_rows();// suento cuantos registro trae la consulta
			if ($cantidad>0) // verifico que sea mayor a 0  
			{// si es mayor a 0 elimino el codigo  que se genero y  retorno el id del cuadrante
				$query="UPDATE unidad_negocio SET codigo  = NULL where codigo = '$codeStorage' ";
				$deleteCode=$this->db->query($query);
				return $selectId->result();
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
		
	}

	/**
	 * la funcion IConfiPaleta se encarga de realizar la insercion de las paletas que fueron definida para el cuadrante.
	 */
	public function IConfiPaleta($id_cuadrante,$paleta)
	{
		$codeStorage= $this->genera_numero();// se genera un codigo aleatorio
		$string="A-B-C-D-E-F-G-H-I-J-K-L-M-N-O-P-Q-R-S-T-U-V-W-X-Y-Z"; // en esta cadena de caracteres almaceno todas las letras del abecedario separado por '-'
		$delimiter=(($paleta[0]*2))-52; // realizo esta formula matematica que me permitiria saber cual sera la delimitacion esta formula consiste en multiplicar la cantidad de paletas que definio crear por 2 y se le resta 52 que es la totalidad de caracteres que tiene el $string alli nos dara el numero que usara $Substr
		$Substr=substr($string, 0 ,$delimiter);// usando la funcion de php substr le defino que va a extraer de $string comenzando desde la posicion 0 y extraera hasta lo que haya resultado en $delimiter
		$array=explode("-", $Substr); // exploro la cadena de caracteres extarida de $string usaundo como parametro divisor '-'

		$count=count($array);// cuento cuantas posiciones tiene el arreglo que resulto de $array

		for ($i=0; $i < $count; $i++) //uso lo contado anteriomente para definirle al for cuantos recorridos hara
		{ 
			$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id)
			VALUES ('$codeStorage','$array[$i]',0,22,4,'$id_cuadrante[0]','196','53')";
			$insertPaleta=$this->db->query($query); // realizo la insercion a la base de datos.
		}


	}


	/**
	 * la funcion insertEstante gestiona la insercion de los estantes en el subalmacen que se esta creando
	 */
	public function insertEstante($almacen,$cantEstante)
	{
		if ($almacen != '') // verifico que al momento de intentar realizar la insercion haya un subalmacen creado para poder realizar la insercion
		{// si existe un subalmacen esta creado
			$codeStorage= $this->genera_numero(); // genero un codigo aleatorio
			for ($i=1; $i <= $cantEstante ; $i++) // verifico la cantidad de estantes que el usuario eligio para crear
			{ 
				$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id)
				VALUES ('$codeStorage','$i',0,22,3,'$almacen','196','53')";
				$insertNewStorage=$this->db->query($query);// realiza x´s cantidad de inserciones segun los estantes definidos por el usuario
			}
			$query="SELECT id FROM unidad_negocio where codigo = '$codeStorage' and empresa_id = $almacen";
			$selectId=$this->db->query($query);// extraigo el id de los cuadrantes
			$cantidad=$selectId->num_rows();// suento cuantos registro trae la consulta
			if ($cantidad>0) // verifico que sea mayor a 0  
			{// si es mayor a 0 elimino el codigo  que se genero y  retorno el id del cuadrante
				$query="UPDATE unidad_negocio SET codigo  = NULL where codigo = '$codeStorage' ";
				$deleteCode=$this->db->query($query);
				return $selectId->result();
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
		
	}

	/**
	 * la funcion IConfiEntrepano se encarga de realizar la insercion de las entrepaños que fueron definida para el esatnte.
	 */
	public function IConfiEntrepano($id_estante,$entrepano)
	{
		$codeStorage= $this->genera_numero();// se genera un codigo aleatorio
		$string="A-B-C-D-E-F-G-H-I-J-K-L-M-N-O-P-Q-R-S-T-U-V-W-X-Y-Z";// en esta cadena de caracteres almaceno todas las letras del abecedario separado por '-'
		$delimiter=(($entrepano[0]*2))-52;// realizo esta formula matematica que me permitiria saber cual sera la delimitacion esta formula consiste en multiplicar la cantidad de paletas que definio crear por 2 y se le resta 52 que es la totalidad de caracteres que tiene el $string alli nos dara el numero que usara $Substr
		$pruebaSubstr=substr($string, 0 ,$delimiter);// usando la funcion de php substr le defino que va a extraer de $string comenzando desde la posicion 0 y extraera hasta lo que haya resultado en $delimiter
		$array=explode("-", $pruebaSubstr);// exploro la cadena de caracteres extarida de $string usaundo como parametro divisor '-'

		$count=count($array);// cuento cuantas posiciones tiene el arreglo que resulto de $array
		
		for ($i=0; $i < $count; $i++)//uso lo contado anteriomente para definirle al for cuantos recorridos hara
		{ 
			$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id,ubcn, empresa_id, pais_id, extras_id)
			VALUES ('$codeStorage','$array[$i]',0,22,5,'$id_estante[0]','196','53')";
			$insertPaleta=$this->db->query($query);// realizo la insercion a la base de datos.
		}


	}

	/**
	 * la funcion insertRack se encarga de realizar la insercion del que fue creado por el usuario.
	 */
	public function insertRack($almacen,$name_rack)
	{
		if ($almacen != '') // verifico que al momento de intentar realizar la insercion haya un subalmacen creado para poder realizar la insercion
		{// si existe un subalmacen esta creado
			$codeStorage= $this->genera_numero();// se genera un codigo aleatorio
			$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id)
			VALUES ('$codeStorage','$name_rack',0,22,6,'$almacen','196','53')";
			$insertNewStorage=$this->db->query($query); // inserto el registro del nombre del rack
			$query="SELECT id FROM unidad_negocio where codigo = '$codeStorage' and empresa_id = $almacen";
			$selectId=$this->db->query($query); // consulto el id del rack creado.
			$cantidad=$selectId->num_rows(); // cuento cuantos registros me devovio la consulta
			if ($cantidad>0) // verifico que sea mayor a 0  
			{// si es mayor a 0 elimino el codigo  que se genero y  retorno el id del cuadrante
				$query="UPDATE unidad_negocio SET codigo  = NULL where codigo = '$codeStorage' ";
				$deleteCode=$this->db->query($query);
				return $selectId->result();
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
		
	}

	/**
	 * la funcion IConfiRack gestiona la insercion de las filas y columnas del rack creado
	 */
	public function IConfiRack($id_rack,$fRack,$cRack)
	{
		for ($i=0; $i < $fRack; $i++) // le defino la cantidad de veces a recorrer con el parametro $frack que es la cantidad de filas que definio el usuario que tendra el rack
		{ 
			$insertf=($i+1);// esta variable se creo para qu sirva de numeracion de fila 
			for ($j=0; $j < $cRack; $j++) // le defino la cantidad de veces a recorrer con el parametro $crack que es la cantidad de columnas que tendra una fila definida por el usuario y que tendra el rack
			{ 	
				$insertc=($j+1);// esta variable se creo para qu sirva de numeracion de columna 
				$data_insert=$insertf." - ".$insertc; //concateno insertf con insertc que sera la nomenclatura de cada espacio dentro del rack
				$query="INSERT INTO unidad_negocio (nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id,fila,columna)
				VALUES ('$data_insert',0,22,7,'$id_rack','196','53','$insertf',$insertc)";
				$insertcrack[$j]=$this->db->query($query); // realizo la insercion de las filas y columnas del rack
			}
		}
	}

	/**
	 * la funcion dataRack me da la informacion de los rack's nombre de rack y id
	 */
	public function dataRack($id)
	{
		$query="SELECT id as codigo,
		nombre as descripcion
		FROM unidad_negocio 
		WHERE id = $id";
		$id=$this->db->query($query);
		return $id->result();
	}

	/**
	 * la funcion adicionalRack me da la informacon de las filas y columnas que tiene el rack seleccionado.
	 */
	public function adicionalRack($id)
	{
		$query="SELECT id as codigo,
		nombre as descripcion
		FROM unidad_negocio
		WHERE empresa_id = $id ";
		$selectEs=$this->db->query($query);
		if ($selectEs->num_rows()>0) {
			foreach ($selectEs->result() as $option)
			{
				$data[]=array(
					$option->codigo,
					$option->descripcion
				);
			}
			return $data;
		}
		else
		{
			return 0;
		}

	}

	/**
	 * la funcion selectCertifica me da la informacion de ubicacion del subalmacen generado, este modelo es el usado cuando ya se realiza la creacion total de un subalmacen.
	 */
	public function selectCertifica($pruebaIdSA)
	{
		$query = "SELECT unidad_negocio.id as idalmacen,
		hospital, 
		estado_id, 
		tipo_id, 
		region_id, 
		ref.referencia as tipo_localidad, 
		ref1.referencia as region, 
		direccion.nombre as estado, 
		establecimientos.id as sedeId,
		unidad_negocio.nombre as nombre
		FROM unidad_negocio
		inner join establecimientos on extras_id = establecimientos.id
		inner join referencia as ref on ref.id = tipo_id
		inner join referencia as ref1 on ref1.id = region_id
		inner join direccion on estado_id = direccion.id
		where  unidad_negocio.id  = $pruebaIdSA";
		$result= $this->db->query($query);

		$cantidad=$result->num_rows();
		if ($cantidad > 0) {
			foreach ($result->result() as $option) {
				$data[] = array( $option->idalmacen, 
					$option->idalmacen,
					$option->hospital,
					$option->estado_id,
					$option->tipo_id,
					$option->region_id,
					$option->tipo_localidad,
					$option->region,
					$option->estado,
					$option->sedeId,
					$option->nombre
				);
			}
			return $data;
		}else{
			return 1000;
		}
	}

	/**
	 * la funcion certificacionCuadrantes ma da la informacion de todos los cuadrantes que fueron generados al momento de a creacion del subalmacen
	 */
	public function certificacionCuadrantes($pruebaIdSA)
	{
		$query="SELECT u2.id as codigoC, 
		u2.nombre as descripcionC, 
		u3.id as codigoP, 
		u3.nombre as descripcionP 
		FROM unidad_negocio as u1
		JOIN unidad_negocio as u2 on  u1.id = u2.empresa_id
		JOIN unidad_negocio as u3 on u2.id = u3.empresa_id
		WHERE u1.id = $pruebaIdSA and u2.ubcn = 2";
		$selectEs=$this->db->query($query);
		if ($selectEs->num_rows()>0) {
			foreach ($selectEs->result() as $option)
			{
				$data[]=array(
					$option->codigoC,
					$option->descripcionC,
					$option->codigoP,
					$option->descripcionP
				);
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * la funcion certificacionEstantes ma da la informacion de todos los estantes que fueron generados al momento de a creacion del subalmacen
	 */
	public function certificacionEstantes($pruebaIdSA)
	{
		$query="SELECT u2.id as codigoC, 
		u2.nombre as descripcionC, 
		u3.id as codigoP, 
		u3.nombre as descripcionP 
		FROM unidad_negocio as u1
		JOIN unidad_negocio as u2 on  u1.id = u2.empresa_id
		JOIN unidad_negocio as u3 on u2.id = u3.empresa_id
		WHERE u1.id = $pruebaIdSA and u2.ubcn = 3";
		$selectEs=$this->db->query($query);
		if ($selectEs->num_rows()>0) {
			foreach ($selectEs->result() as $option)
			{
				$data[]=array(
					$option->codigoC,
					$option->descripcionC,
					$option->codigoP,
					$option->descripcionP
				);
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}

	/**
	 * la funcion certificacionRack ma da la informacion de todos los rack que fueron generados al momento de a creacion del subalmacen
	 */
	public function certificacionRack($pruebaIdSA)
	{
		$query="SELECT u2.id as codigoC, 
		u2.nombre as descripcionC, 
		u3.id as codigoP, 
		u3.nombre as descripcionP
		FROM unidad_negocio as u1
		JOIN unidad_negocio as u2 on  u1.id = u2.empresa_id
		JOIN unidad_negocio as u3 on u2.id = u3.empresa_id
		WHERE u1.id = $pruebaIdSA and u2.ubcn = 6 order by u2.nombre ASC";
		$selectEs=$this->db->query($query);
		if ($selectEs->num_rows()>0) {
			foreach ($selectEs->result() as $option)
			{
				$data[]=array(
					$option->codigoC,
					$option->descripcionC,
					$option->codigoP,
					$option->descripcionP
				);
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}
}
?>