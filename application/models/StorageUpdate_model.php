<?php 

class StorageUpdate_model extends CI_Model 
{

	/**
	 * la funcion genera_numero es usada para generar uncodigo aleatorio que sera usada como codigo en los diferentes niveles de almacenamiento, esta funcion genera un numero unico.
	 */
	public function genera_numero()
	{
		$codigo=rand(1,9999);//rand que genera el numero aleatorio
		$data=$this->no_duplic_num_solicitudC($codigo);// aqui se hace referencia al metodo no_duplic_num_solicitudC pasando el rand que se fue creado.
		return $data;
	}

	/**
	 * la funcion no_duplic_num_solicitudC se encarga de validar que el codigo que se genero anteriormente no este duplicado en la base de datos
	 */
	public function no_duplic_num_solicitudC($codigo)
	{
		$data=$this->no_duplic_num_solicitud($codigo);//aqui se hace referencia al metodo no_duplic_num_solicitud pasando el codigo recibido por parametro.
		if ($data == 1) //pregunto si el resultado del llamado que se hizo anteriormente es igual a 1
		{// si es igual a uno vuelve a generar un codigo aleatorio nuevamente
			$duplicado=$this->genera_numero();
		}
		else
		{ //si no retorna el codigo
			return $codigo;
		}
	}


	/**
	 * la funcion no_duplic_num_solicitud es la funcion que se encarga de verificar con la base de datos que el codigo que fue generado en la funcion genera_numero se encuentre registrada
	 */
	public function no_duplic_num_solicitud($codigo)
	{
		$registro="SELECT codigo FROM unidad_negocio where codigo = '$codigo'";
		$query=$this->db->query($registro);
		$numero=$query->num_rows();// cuento las filas que retorna la consulta.
		if ($numero > 0) // aqui verifica si el resultado de la consulta trae mas de un registro
		{// si el resultado es mayor a 0 entonces retorno 1
			return 1;
		}
		else
		{ // si no retorno 0;
			return 0;
		}
	}
	
	/**
	 * la funcion SubalmacenU actualiza el nombre del subalmacen
	 */
	public function SubalmacenU($nameStorage,$id_sa,$almacen)
	{
		$query="UPDATE unidad_negocio SET nombre = '$nameStorage' WHERE id = $id_sa and empresa_id=$almacen";
		$UpdateNewStorage=$this->db->query($query);
		
		if ($insertNewStorage = true) // pregunta si la actualizacion fue exitosa
		{ // si fue exitosa retorna un 1 que sera usado para dar el mensaje de actualizacion exitosa
			return 1;
		}
		else
		{// si no retornara un 0 para el mensaje de error
			return 0;
			
		}
	}

	/**
	 * la funcion datSA me da el nombre del subalmacen en especifico
	 */
	public function datSA($pruebaIdSA)
	{
		$query = "  SELECT id as idalmacen,
		nombre as nombre
		FROM unidad_negocio
		WHERE  id  = $pruebaIdSA";
		$result= $this->db->query($query);

		$cantidad=$result->num_rows();
		if ($cantidad > 0) {
			foreach ($result->result() as $option) {
				$data[] = array( $option->idalmacen, 
					$option->nombre
				);
			}
			return $data;
		}else{
			return 1000;
		}
	}

 	/**
 	 * la funcion cuadrantesExistentes me da la cantidad de cuadrantes que tiene creado ese subalmacen
 	 */
 	public function cuadrantesExistentes($pruebaIdSA)
 	{
 		$query=" SELECT id, nombre from unidad_negocio where empresa_id = $pruebaIdSA and ubcn =2";
 		$selectEs=$this->db->query($query);
 		if ($selectEs->num_rows()>0) {
 			foreach ($selectEs->result() as $option)
 			{
 				$data[]=array(
 					$option->id,
 					$option->nombre
 				);
 			}
 			return $data;
 		}
 		else
 		{
 			return 0;
 		}
 	}

	/**
 	 * la funcion estantesExistentes me da la cantidad de estantes que tiene creado ese subalmacen
 	 */
	public function estantesExistentes($pruebaIdSA)
	{
		$query=" SELECT id, nombre from unidad_negocio where empresa_id = $pruebaIdSA and ubcn =3";
		$selectEs=$this->db->query($query);
		if ($selectEs->num_rows()>0) {
			foreach ($selectEs->result() as $option)
			{
				$data[]=array(
					$option->id,
					$option->nombre
				);
			}
			return $data;
		}
		else
		{
			return 0;
		}
	}

	/**
 	 * la funcion rackE actualiza el nombre del rack del subalamcen seleccionado
 	 */
	public function rackE($id_espacioRack,$name)
	{
		$query="UPDATE unidad_negocio SET nombre = $name WHERE id = $id_espacioRack";
		$updateEspRack=$this->db->query($query);
		if ($updateEspRack)// pregunta si la actualizacion fue exitosa
		{ // si fue exitosa retorna un 1 que sera usado para dar el mensaje de actualizacion exitosa
			return 1;
		}
		else
		{// si no retornara un 0 para el mensaje de error
			return 0;
		}
	} 

	/**
	 * la funcion imgSA cme da la imagen que fue cargada para el subalmacen 
	 */
	public function imgSA($id_subalmacen)
	{
		if ($id_subalmacen == 0) //pregunto si el parametro que se envio del controlador es igual a 0
		{// si el valor es 0 entonces le devuelvo un arreglo con valor 0 para indicarle que no hay un subalmacen creado
			$data[]=array (0);
			return $data;
		}
		else
		{//si no, ejecuta la consulta
			$query="SELECT img 
			FROM unidad_negocio
			WHERE id = $id_subalmacen";
			$selecyImg=$this->db->query($query);// consulto la ruta que se almaceno de la imagen
			$cantidad=$selecyImg->num_rows(); // cuento la cantidad de registros que retorno la consulta
			$result=$selecyImg->result(); // esatrigo los datos de la consulta
			if (($cantidad > 0) and ($result[0]->img != NULL))//pregunto si la cantidad es mayor a 0 y si trajo algun dato la consulta
			{// si se cumplen las dos condiciones retorno el valor de la consulta
				foreach ($result as $option) 
				{
					$data[]=array(
						$option->img
					);

				}
				return $data;
			}
			else
			{
				return 0;
			}
		}
		
	}

	/**
	 * la funcion imgSA cme da la imagen que fue cargada para el subalmacen cuando se esta en la creacion de cuadrantes, estantes y rack
	 */
	public function imgSAp($id_subalmacen)
	{
		$query="SELECT img 
		FROM unidad_negocio
		WHERE id = $id_subalmacen";
		$selecyImg=$this->db->query($query);
		$cantidad=$selecyImg->num_rows();
		if (($cantidad > 0) and ($selecyImg->result() != NULL))
		{
			foreach ($selecyImg->result() as $option) 
			{
				$data[]=array(
					$option->img
				);
				return $data;
			}

		}
		else
		{
			return 0;
		}
		
	}

	/**
	 * la funcion infoSA me da el nombre del subalmacen como informacion para el mmomento de consultar o visualizar un subalmacen
	 */
	public function infoSA($subalmacen)
	{
		$query="SELECT nombre FROM unidad_negocio WHERE id=$subalmacen";
		$info=$this->db->query($query);
		return $info->result();
	}

	/**
	 * la funcion selectCuadrante me da lso cuadrantes que existen en en subalmacen retornando el ultimno cuadrante agregado
	 */
	public function selectCuadrante($subalmacen)
	{
		$query="SELECT id, nombre 
		FROM unidad_negocio 
		WHERE empresa_id = $subalmacen and ubcn=2 
		order by id desc limit 1";
		$ultimo_cuadrante=$this->db->query($query);// consulto el ultimo registro
		return $ultimo_cuadrante->result();// lo envio al metodo que lo este invocando.
	}

	/**
	 * la funcion insertCuadranteU gestiona la insercion de nuevos cuadrantes cuando en se esta editando un subalmacen y se quiere agregar un nuevo cuadrante
	 */
	public function insertCuadranteU($almacen,$cantCuadrante)
	{
		$cuadrantesExiste=$this->selectCuadrante($almacen);// consulto cual es el ultimo cuadrante creado 
		if ($almacen != '') // verifico que al momento de intentar realizar la insercion haya un subalmacen creado para poder realizar la insercion
		{// si existe un subalmacen creado
			$cont=($cuadrantesExiste[0]->nombre)+1; //sle sumo al valor del ultimo cuadrante 1 para conocer cual es el delimitador del for
			$codeStorage= $this->genera_numero();// genero un codigo aleatorio
			for ($i=$cont; $i <= $cantCuadrante ; $i++) //recorro el for tomando en cuenta como mi base el la cuenta que se calculo anteriormente y la condicion sera que se menor o igual a la cantidad de cuadrantes que selecciono el usuario
			{ 
				$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id)
				VALUES ('$codeStorage','$i',0,22,2,'$almacen','196','53')";
				$insertNewStorage=$this->db->query($query);// inserto los valores
			}
			$query="SELECT id FROM unidad_negocio where codigo = '$codeStorage' and empresa_id = $almacen";
			$selectId=$this->db->query($query);//consulto el id del cuadrante
			$cantidad=$selectId->num_rows();// cuento cuantos registros me devuelve la consulta
			if ($cantidad>0) // verifico si es mayor a 0
			{// si es mayor a 0 elimino el codigo  que se genero y  retorno el id del cuadrante
				$query="UPDATE unidad_negocio SET codigo  = NULL where codigo = '$codeStorage' ";
				$deleteCode=$this->db->query($query);
				return $selectId->result();
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
		
	}

	/**
	 * la funcion selectIdCuadrante me da los datos del cuadrante con el que se crearan las paletas.
	 */
	public function selectIdCuadrante($subalmacen)
	{
		$query="SELECT id, nombre 
		FROM unidad_negocio 
		WHERE empresa_id = $subalmacen and ubcn=2";
		$ultimo_cuadrante=$this->db->query($query);
		return $ultimo_cuadrante->result();
	}

	/**
	 * la funcion detalles em devuelve el nombre del cuadrante que se escogio
	 */
	public function detalles($id)
	{
		$query="SELECT nombre 
		FROM unidad_negocio 
		WHERE id = $id and ubcn=2";
		$ultimo_cuadrante=$this->db->query($query);
		$cantidad=$ultimo_cuadrante->num_rows();
		if ($cantidad > 0)
		{
			foreach ($ultimo_cuadrante->result() as $option) 
			{
				$data[]=array(
					$option->nombre
				);
				return $data;
			}

		}
		else
		{
			return 0;
		}
	}

	/**
	 * la funcion letra_num me permite recibir una letra y dependiendo de la letra que se pase por parametros retorna el valor en numero
	 */
	public function letra_num($letra)
	{
		if ($letra == "A") 
			{ $num=1;} 
		elseif ($letra == "B") 
			{$num=2;}
		elseif ($letra == "C") 
			{$num=3;}
		elseif ($letra == "B") 
			{$num=4;}
		elseif ($letra == "D") 
			{$num=5;}
		elseif ($letra == "E") 
			{$num=6;}
		elseif ($letra == "F") 
			{$num=7;}
		elseif ($letra == "G") 
			{$num=8;}
		elseif ($letra == "H") 
			{$num=9;}
		elseif ($letra == "I") 
			{$num=10;}
		elseif ($letra == "J") 
			{$num=11;}
		elseif ($letra == "K") 
			{$num=12;}
		elseif ($letra == "L") 
			{$num=13;}
		elseif ($letra == "M") 
			{$num=14;}
		elseif ($letra == "N") 
			{$num=15;}
		elseif ($letra == "O") 
			{$num=16;}
		elseif ($letra == "P") 
			{$num=17;}
		elseif ($letra == "Q") 
			{$num=18;}
		elseif ($letra == "R") 
			{$num=19;}
		elseif ($letra == "S") 
			{$num=20;}
		elseif ($letra == "T") 
			{$num=21;}
		elseif ($letra == "U") 
			{$num=22;}
		elseif ($letra == "V") 
			{$num=23;}
		elseif ($letra == "W") 
			{$num=24;}
		elseif ($letra == "X") 
			{$num=25;}
		elseif ($letra == "Y") 
			{$num=26;}
		elseif ($letra == "Z") 
			{$num=27;}
		return $num;
	}

	/**
	 * la funcion load_name recibe como parametro el id del cuadrante y  me permite saber la ultima paleta que fue creada para crear mas dentro de  un cuadrante en especifico.
	 */
	public function load_name($id) 
	{
		$query = "SELECT id, nombre 
		FROM unidad_negocio 
		where empresa_id = $id and ubcn=4 order by id desc limit 1";
		$result = $this->db->query($query);//consulto el registro de la ultima paleta creada.
		$valor = $result->num_rows();// cuento cuantos registro me devuelve la consulta.
		if ($valor > 0) //verifico que el valor sea mayor a 0
		{ //si lo es entonces convierto el valor de la paleta en numero para armar el for desde la ultima paleta que fue creada.
			foreach ($result->result() as $option) 
			{
				$numero=$this->letra_num($option->nombre);
				$num=$numero;

				for ($i=$num; $i <= 27; $i++) 
				{
					$data[] = "<option value='".$i."''>".$i."</option>";
				}
			}
			return $data;
		} 
		else 
			{//si no arma el for desde 1
				for ($i=1; $i <= 27; $i++) 
				{
					$data[] = "<option value='".$i."''>".$i."</option>";
				}
				return $data;
			}

		}


	/**
	 * la funcion IConfiPaletaU gestiona la ainsercion de la paleta en la base de datos
	 */
	public function IConfiPaletaU($id,$paleta)
	{
		$query = "SELECT nombre 
		FROM unidad_negocio 
		where empresa_id = $id and ubcn=4 order by id desc limit 1";
		$result = $this->db->query($query);// consulto el ultimo registro de la paleta
		$ext=$result->result();//almaceno el valor en una variable
		$letra=$ext[0]->nombre;//extraigo el nombre
		$numero=($this->letra_num($letra)*2); //hago el llamado  a letra numero para saber que valor tiene lo que trajo de la consulta y poder realizar la formula matematica.

		$codeStorage= $this->genera_numero();//genera un codigo aleatorio

		$string="A-B-C-D-E-F-G-H-I-J-K-L-M-N-O-P-Q-R-S-T-U-V-W-X-Y-Z";// en esta cadena de caracteres almaceno todas las letras del abecedario separado por '-'
		$delimiter=(($paleta[0]*2))-52;// realizo esta formula matematica que me permitiria saber cual sera la delimitacion esta formula consiste en multiplicar la cantidad de paletas que definio crear por 2 y se le resta 52 que es la totalidad de caracteres que tiene el $string alli nos dara el numero que usara $Substr
		$pruebaSubstr=substr($string, $numero,$delimiter);// usando la funcion de php substr le defino que va a extraer de $string comenzando desde la posicion 0 y extraera hasta lo que haya resultado en $delimiter
		$array=explode("-", $pruebaSubstr);// exploro la cadena de caracteres extarida de $string usaundo como parametro divisor '-'
		$count=count($array);
		//die(var_dump($array));
		for ($i=0; $i < $count; $i++) //se recorre el ciclo la cantidad de veces que valores tenga el arreglo
		{ 
			$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id)
			VALUES ('$codeStorage','$array[$i]',0,22,4,'$id','196','53')";
			$insertPaleta=$this->db->query($query);//realizo la insercion
		}


	}

	/**
	 * la funcion selectEstante me da los estantes que existen en en subalmacen retornando el ultimo cuadrante agregado
	 */
	public function selectEstante($subalmacen)
	{
		$query="SELECT id, nombre 
		FROM unidad_negocio 
		WHERE empresa_id = $subalmacen and ubcn=3 
		order by id desc limit 1";
		$ultimo_estante=$this->db->query($query);// consulto el ultimo registro
		return $ultimo_estante->result();// lo envio al metodo que lo este invocando.
	}

	/**
	 * la funcion selectIdEstante me da los datos del estante con el que se crearan los entrepaños.
	 */
	public function selectIdEstante($subalmacen)
	{
		$query="SELECT id, nombre 
		FROM unidad_negocio 
		WHERE empresa_id = $subalmacen and ubcn=3";
		$ultimo_estante=$this->db->query($query);
		return $ultimo_estante->result();
	}

	/**
	 * la funcion insertEstanteU gestiona la insercion de nuevos estantes cuando en se esta editando un subalmacen y se quiere agregar un nuevo estante
	 */
	public function insertEstanteU($almacen,$cantEstante)
	{
		$estantesExiste=$this->selectEstante($almacen);//invoco al metodo selectEstante para verificar cual fue el ultimo estante que fue creado.
		if ($almacen != '')// verifico que al momento de intentar realizar la insercion haya un subalmacen creado para poder realizar la insercion
		{// si existe un subalmacen creado
			$cont=($estantesExiste[0]->nombre)+1;//le sumo al valor del ultimo cuadrante 1 para conocer cual es el delimitador del for
			$codeStorage= $this->genera_numero();//genero un codigo aleatorio
			for ($i=$cont; $i <= $cantEstante ; $i++) //recorro el for tomando en cuenta como mi base el la cuenta que se calculo anteriormente y la condicion sera que se menor o igual a la cantidad de estante que selecciono el usuario
			{ 
				$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id)
				VALUES ('$codeStorage','$i',0,22,3,'$almacen','196','53')";
				$insertNewStorage=$this->db->query($query);
			}
			$query="SELECT id FROM unidad_negocio where codigo = '$codeStorage' and empresa_id = $almacen";
			$selectId=$this->db->query($query);
			$cantidad=$selectId->num_rows();
			if ($cantidad>0) // verifico si es mayor a 0
			{// si es mayor a 0 elimino el codigo  que se genero y  retorno el id del cuadrante
				$query="UPDATE unidad_negocio SET codigo  = NULL where codigo = '$codeStorage' ";
				$deleteCode=$this->db->query($query);
				return $selectId->result();
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
		
	}

	/**
	 * la funcion detallesE sirve para consultar el nombre del estante que se selecciono a editar
	 */
	public function detallesE($id)
	{
		$query="SELECT nombre 
		FROM unidad_negocio 
		WHERE id = $id and ubcn=3";
		$ultimo_cuadrante=$this->db->query($query);
		$cantidad=$ultimo_cuadrante->num_rows();
		if ($cantidad > 0)
		{
			foreach ($ultimo_cuadrante->result() as $option) 
			{
				$data[]=array(
					$option->nombre
				);
				return $data;
			}

		}
		else
		{
			return 0;
		}
	}

	/**
	 * la funcion load_name recibe como parametro el id del estante y  me permite saber el ultimo que fue creado para crear mas dentro de  un estante en especifico.
	 */
	public function load_nameE($id) 
	{
		$query = "SELECT id, nombre 
		FROM unidad_negocio 
		where empresa_id = $id and ubcn=5 order by id desc limit 1";
		$result = $this->db->query($query);
		$valor = $result->num_rows();// cuento cuantos registro me devuelve la consulta.
		if ($valor > 0) //verifico que el valor sea mayor a 0
		{ //si lo es entonces convierto el valor del entrepaño en numero para armar el for desde el ultimo que fue creada.
			foreach ($result->result() as $option) 
			{
				$numero=$this->letra_num($option->nombre);// invoco a la funcion letra_num pasando por parametro el nombre que tiene ese entrepaño en la base de datos para que me devuelva el valor numerico por el cual me voy a guiar para armar el for
				$num=$numero;
				for ($i=$num; $i <= 27; $i++) 
				{
					$data[] = "<option value='".$i."''>".$i."</option>";
				}
			}
			return $data;
		} 
		else 
		{// si no arma el for desde 1
			for ($i=1; $i <= 27; $i++) 
			{
				$data[] = "<option value='".$i."''>".$i."</option>";
			}
			return $data;
		}

	}

	/**
	 * la funcion IConfiEntrepanoU gestiona la insercion del cuadrante en la base de datos
	 */
	public function IConfiEntrepanoU($id,$entrepano)
	{
		$query = "SELECT nombre 
		FROM unidad_negocio 
		where empresa_id = $id and ubcn= 5 order by id desc limit 1";
		//die(var_dump($query));
		$result = $this->db->query($query);// consulto el ultimo registro de la paleta
		$ext=$result->result();//almaceno el valor en una variable
		$letra=$ext[0]->nombre;//extraigo el nombre
		$numero=($this->letra_num($letra)*2);//hago el llamado  a letra numero para saber que valor tiene lo que trajo de la consulta y poder realizar la formula matematica.

		$codeStorage= $this->genera_numero();// genero un codigo aleatorio
		$string="A-B-C-D-E-F-G-H-I-J-K-L-M-N-O-P-Q-R-S-T-U-V-W-X-Y-Z";// en esta cadena de caracteres almaceno todas las letras del abecedario separado por '-'
		$delimiter=(($entrepano[0]*2))-52;/// realizo esta formula matematica que me permitiria saber cual sera la delimitacion esta formula consiste en multiplicar la cantidad de paletas que definio crear por 2 y se le resta 52 que es la totalidad de caracteres que tiene el $string alli nos dara el numero que usara $Substr
		$pruebaSubstr=substr($string, $numero ,$delimiter);/// usando la funcion de php substr le defino que va a extraer de $string comenzando desde la posicion 0 y extraera hasta lo que haya resultado en $delimiter
		$array=explode("-", $pruebaSubstr);/// exploro la cadena de caracteres extarida de $string usaundo como parametro divisor '-'
		$count=count($array);

		for ($i=0; $i < $count; $i++) //se recorre el ciclo la cantidad de veces que valores tenga el arreglo
		{ 
			$query="INSERT INTO unidad_negocio ( codigo, nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id)
			VALUES ('$codeStorage','$array[$i]',0,22,5,'$id','196','53')";
			$insertEntrepano=$this->db->query($query);
		}
		$query="SELECT id FROM unidad_negocio where codigo = '$codeStorage' and empresa_id = $id";
			$selectId=$this->db->query($query);
			$cantidad=$selectId->num_rows();
			if ($cantidad>0) // verifico si es mayor a 0
			{// si es mayor a 0 elimino el codigo  que se genero y  retorno el id del cuadrante
				$query="UPDATE unidad_negocio SET codigo  = NULL where codigo = '$codeStorage' ";
				$deleteCode=$this->db->query($query);
				return $selectId->result();
			}
			else
			{
				return 0;
			}
	}

	/**
	* la funcion selectRack me da los racks que existen en en subalmacen retornando el ultimo rack agregado
	*/

	public function selectRack($subalmacen)
	{
		$query="SELECT id, nombre 
		FROM unidad_negocio 
		WHERE empresa_id = $subalmacen and ubcn=6 
		order by id desc limit 1";
		$ultimo_rack=$this->db->query($query);
		return $ultimo_rack->result();
	}

	/**
	* la funcion selectIdRack me da los datos del rack con el que se crearan los espacios.
	*/

	public function selectIdRack($subalmacen)
	{
		$query="SELECT id, nombre 
		FROM unidad_negocio 
		WHERE empresa_id = $subalmacen and ubcn=6";
		$ultimo_cuadrante=$this->db->query($query);
		return $ultimo_cuadrante->result();
	}

	/**
	 * la funcion load_nameR recibe como parametro el id del rack y  me permite saber el ultimo que fue creado para crear mas dentro de  un rack en especifico.
	 */
	public function load_nameR($id) 
	{
		$query = "SELECT fila 
		FROM unidad_negocio 
		where empresa_id = $id and ubcn=7 order by id desc limit 1";
		$result = $this->db->query($query);
		$valor = $result->num_rows();
		if ($valor > 0) //verifico que el valor sea mayor a 0
		{ //si lo es entonces le muestro en la vista la ultima fila  
			foreach ($result->result() as $option) 
			{
				$numero=(($option->fila)+1);

				$data[] = "<input type='number' name='fRack' id='fRack' min='".$numero."' class='form-control' placeholder='".$numero."' value='".$numero."'>
				<input type='hidden' name='fRackh' id='fRackh' value='".$option->fila."'>";
			}
			return $data;
		} else {
			$msj = "<option value='0'>Seleccione..</option>";
			return $msj;
		}

	}

	/**
	 * la funcion IConfiRackU inserta en la base de datos las cantidad de columnas que haya definido el usuario
	 */
	public function IConfiRackU($id_rack,$fRack,$cRack,$fRackh)
	{

		for ($i=$fRackh; $i < $fRack; $i++) 
		{ 
			$insertf=($i+1);
			for ($j=0; $j < $cRack; $j++) 
			{ 	
				$insertc=($j+1);
				$data_insert=$insertf." - ".$insertc;
				$query="INSERT INTO unidad_negocio (nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id,fila,columna)
				VALUES ('$data_insert',0,22,7,'$id_rack','196','53','$insertf',$insertc)";
				$insertcrack[$j]=$this->db->query($query);
			}
		}
	}

	/**
	 * la funcion selectIdRowRack consulta la ultima columna qyu fue creada en una fila especifica.
	 */
	public function selectIdRowRack($id_rack)
	{
		$query="SELECT id, nombre 
		FROM unidad_negocio 
		WHERE empresa_id = $subalmacen and ubcn=6";
		$ultimo_cuadrante=$this->db->query($query);
		return $ultimo_cuadrante->result();
	}

	/**
	 * la funcion load_nameCR consulta y arma el select de las filas que se encuentran disponibles para modificar
	 */
	public function load_nameCR($id) 
	{
		$query = "SELECT DISTINCT(fila) 
		FROM unidad_negocio 
		where empresa_id = $id and ubcn=7 ";
		$result = $this->db->query($query);
		$valor = $result->num_rows();
		if ($valor > 0) {
			foreach ($result->result() as $option) 
			{

				$data[] = "<option value='".$option->fila."''>".$option->fila."</option>";

			}
			return $data;
		} else {
			$msj = "<option value='0'>Seleccione..</option>";
			return $msj;
		}

	}

	/**
	 * la funcion IConfiRackUI gestiona la insercion de las columnas que agrego en la fila seleccionada
	 */
	public function IConfiRackUI($id_rack,$fRack,$cRack,$cRackh)
	{

		for ($j=$cRackh; $j < $cRack; $j++) 
		{ 	
			$insertc=($j+1);
			$data_insert=$fRack." - ".$insertc;
			$query="INSERT INTO unidad_negocio (nombre, desactivada, modelo_has_submodelo_id, ubcn, empresa_id, pais_id, extras_id,fila,columna)
			VALUES ('$data_insert',0,22,7,'$id_rack','196','53','$fRack','$cRack')";
			$insertcrack[$j]=$this->db->query($query);
		}
	}

	/**
	 * la funcion deleteCER es la encargada de gestionar los entrepaños, paletas o espacios del rack(fila-columna) que se van a eliominar, siempre verificando que ninguna de las ubicaciones tenga mercancia
	 */
	public function deleteCER($id,$pruebaIdSA,$idcer)
	{
		$query="SELECT sum(existencia) as existencia 
		FROM mercancia_has_unidad_negocio 
		WHERE cuadrante_estante_id = $idcer 
		and subalmacen_id=$pruebaIdSA and unidad_negocio_id = $id
		ORDER BY id DESC";
		$verifyExistencia=$this->db->query($query);// este query se encarga de verifiacr si hay existencia de mercancia en la ubicacion a eliminar.
		$result=$verifyExistencia->result();
		if (($result[0]->existencia) == 0 or ($result[0]->existencia) == NULL) // verifico que efectivamente no posea mercancia alli
		{//si no hay mercancia en esa ubicacion la elimino
			$query="DELETE FROM unidad_negocio WHERE id = $id and empresa_id = $idcer";
				//die(var_dump($query));
			$delete=$this->db->query($query);
			if ($delete) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}

	/**
	 * la funcion deleteSCER es la encargada de gestionar los estantes, cuadrantes o rack que se van a eliominar, siempre verificando que ninguna de las ubicaciones tenga mercancia
	 */
	public function deleteSCER($id,$pruebaIdSA,$idcer)
	{
		$query="SELECT sum(existencia) as existencia 
		FROM mercancia_has_unidad_negocio 
		WHERE cuadrante_estante_id = $idcer 
		and subalmacen_id=$pruebaIdSA and unidad_negocio_id = $id
		ORDER BY id DESC";
		$verifyExistencia=$this->db->query($query);// este query se encarga de verifiacr si hay existencia de mercancia en la ubicacion a eliminar.
		$result=$verifyExistencia->result();

		$query="SELECT id 
		FROM unidad_negocio 
		WHERE empresa_id = $idcer
		ORDER BY id DESC";
		$verifyExistenciaPE=$this->db->query($query);// esta consulta verifica si el cuadrante, estante o rack posee creado una paleta, entrepaño o espacio (filas)
		$resultPE=$verifyExistenciaPE->result();
		if ((($result[0]->existencia) == 0 or ($result[0]->existencia) == NULL) and (($resultPE == '') or ($resultPE == NULL)))// verifico que no tenga mercancia en la ubicacion y que no tenga una paleta, en trepaño o espacio
		{// si no tiene existencia ni ninguna paleta, entrepaño o espacio y borro
			$query="DELETE FROM unidad_negocio WHERE id = $id and empresa_id = $pruebaIdSA";
				//die(var_dump($query));
			$delete=$this->db->query($query);
			if ($delete) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}

	/**
	 * la funcion deleteSA es la encargada de gestionar el subalmacen que se van a eliominar, siempre verificando que ninguna de las ubicaciones tenga mercancia
	 */
	public function deleteSA($id)
	{
		$query="SELECT sum(existencia) as existencia 
		FROM mercancia_has_unidad_negocio 
		WHERE subalmacen_id=$id
		ORDER BY id DESC";
		$verifyExistencia=$this->db->query($query);// query para verificar existencia
		$result=$verifyExistencia->result();
		if (($result[0]->existencia) == 0 or ($result[0]->existencia) == NULL) // verifico que no tenga mercancia ninguno de las ubicacioones
		{// si no hay mercancia elimino
			$query="DELETE FROM unidad_negocio WHERE id = $id";
				//die(var_dump($query));
			$delete=$this->db->query($query);
			if ($delete) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}

	/**
	 * la funcion loadColumn consulta cula fue la ultima columna configurada en una fila
	 */
	public function loadColumn($id,$fila) 
	{
		$query = "SELECT DISTINCT(columna) 
		FROM unidad_negocio 
		where empresa_id = $id and ubcn=7 and fila=$fila order by id desc limit 1";
		$result = $this->db->query($query);
		$valor = $result->num_rows();
		$numero=$result->result();
		$pru=$numero[0]->columna;
		if ($valor > 0) {
			foreach ($result->result() as $option) 
			{

				$data[] = "<input type='number' name='cRack' id='cRack' min='".$pru."' class='form-control' placeholder='".$pru."' value='".$pru."'>
				<input type='hidden' name='cRackh' id='cRackh' value='".$option->columna."'>";

			}
			return $data;
		} else {
			$msj = "<option value='0'>Seleccione..</option>";
			return $msj;
		}

	}
}
?>