<div class="col-md-12 mt-4 text-center">
	<div class="row border__table">
		<label for="" class="label__blue ml-5">RACK: <?php echo $name_rack[0]->descripcion; ?></label>
		<table id="example" class="table table-striped table-bordered" style="width:100%">
			<thead>
				<tr>
					<th class="title--bg">Nro</th>
					<th class="title--bg">Fila - Columna</th>
					<th class="title--bg">Acciones</th>
				</tr>
			</thead>
			<tbody  style="text-align: center;">
			<?php 
				
				$i=1;
					foreach ($espacios as $key) 
					{
			?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo $key[1]; ?></td>
							<td><button class="badge badge-info" onclick="eraseEspacio('<?php echo base_url();?>','<?php echo $key[0]; ?>');"><i class="fa fa-trash"></i></button></td>
						</tr>
			<?php
					}

			?>
			</tbody>
		</table>

	</div>
				<a class="btn btn-primary" onclick="siguienteConfirm('<?php echo base_url();?>');">Siguiente</a>
</div>

