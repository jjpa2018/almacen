<div class="dataTable_wrapper">
	<div class="table-responsive col-sm-12">
		<div style="float: right; padding: 0% 0% 2% 0%;">
			<button type="button" class="btn btn-info ml-3" data-toggle="modal" data-target="#subAlmacen1" onclick="modal('<?= base_url(); ?>','1');">
				<i class="fa fa-plus"></i>
			</button>
		</div>
		<table class="dt table table-sm table-striped table-hover table-bordered"> 
			<thead style="text-transform: uppercase;" class="theadH">
				<tr>
					<th><strong>ALMACENES</strong></th>
					<th width="150"><strong>ACCIONES</strong></th>
				</tr>
			</thead>
			<tbody>
				<?php
				if ($almacen != 1000) {
					foreach ($almacen as $key) 
					{
						
						?> 
						<tr>
							<td>
								<a onclick="loadSubStorage('<?php echo base_url();?>','<?php echo $key[0]; ?>');"><?php echo $key[1];?></a>
							</td>
							<td width="150">
								<button class="btn btn-outline-info btn-sm"  data-toggle="modal" data-target="#subAlmacen1" onclick="search('<?php echo base_url(); ?>','<?php echo $key[0]; ?>',2);"><i class="fa fa-search"></i></button>
								<button class="btn btn-outline-warning btn-sm " data-toggle="modal" data-target="#subAlmacen1" onclick="edit('<?php echo base_url(); ?>','<?php echo $key[0]; ?>',2);"><i class="fa fa-pencil"></i></button>
								<button class="btn btn-outline-danger btn-sm " data-toggle="modal" data-target="#subAlmacen1" onclick="deletes('<?php echo base_url(); ?>','<?php echo $key[0]; ?>',2);"><i class="fa fa-trash"></i></button>
							</td>
						</tr>
						<?php
					}
				}
				else
					{ ?>
						<tr>
							<td colspan="2">
								<div class="alert alert-danger alert-dismissible fade show" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									<strong>Este almacén no posee sub-almacenes</strong>
								</div>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
</div>
