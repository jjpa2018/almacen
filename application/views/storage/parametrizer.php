<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="section__title">
					configuracion del sub almacen recepcion del almacen mmq
				</div>
				<h3 class="text-center"></h3>
				<div class="col-md-12">
					<div class="col-lg-offset-5 col-lg-12">
						<div align="center" class="col-lg-12">
							<div class="fileupload fileupload-new" data-provides="fileupload">
								<div class="fileupload-new thumbnail" style="width: 400px; height: 400px;">
									<?php 
									if (!empty($imgSA)) 
										{ 
											?>
											<img src="<?php echo $imgSA[0][0]?>" style="width: 400px; height: 400px; background:  #ebedef ;" alt="" />
											<?php 
										}
										else
										{
											?>
											<img src="<?=base_url().'img/no-image.png'?>" style="width: 400px; height: 400px; background:  #ebedef ;" alt="" />
											<div class="alert alert-danger alert-dismissible fade show" align="center" role="alert">
												<strong>No existe ninguna Imagén para el sub-almacen</strong>
											</div>
											<?php 
										} ?>

									</div>
								</div>
							</div>
						</div>
					</div>
					<br>
					<br>
					<div class="section__title">
						CONFIGURACIONES
					</div>
					<div class="nav nav-tabs">
						<div class="card-header">
							<ul class="nav nav-tabs card-header-tabs">
								<li class="nav-item">
									<a class="nav-link" href="#home" onclick="confi('<?=base_url()?>','1');">CUADRANTE</a><span class="nro">1</span>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#home" onclick="confi('<?=base_url()?>','2');">ESTANTE</a><span class="nro">2</span>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#home" onclick="confi('<?=base_url()?>','3');">RACK</a><span class="nro">3</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="panel-body">
						<div id="home">
							<div id="view_confi">

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>