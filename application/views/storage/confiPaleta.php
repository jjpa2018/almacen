<div class="row">
	<div class="col-md-12" align="center">
		<label class="toUpper">ESPECIFIQUE EL NUMERO DE PALETAS POR CUADRANTE</label>
	</div>
	<?php 
	if (empty($datos)) 
	{

		for ($i=0; $i < $cantCuadrante; $i++) 
			{ ?>
				<div class="col-md-3"> 
					<label><strong><?php echo $i+1; ?></strong></label>
					<div class="square yellow__square"></div>
					<input type="hidden" checked="checked" name="id_cuadrante" id="<?php echo 'id_cuadrante'.$i;?>" value="<?php echo $insert[$i]->id;?>">
					<select name="paleta" id="<?php echo 'paleta'.$i;?>" class="form-control" style="display: inline-block; width: 65px">
						<?php 
						for ($j=1; $j <= 24 ; $j++) 
						{ 
							?>
							<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<?php
			}
		}
		else
		{
			$cont=($datos[0]->nombre);
			for ($i=$cont; $i < $cantCuadrante; $i++) 
			{ 
				$k=$i-$cont;
				?>
				<div class="col-md-3">
					<label><strong><?php echo $i+1; ?></strong></label>
					<div class="square yellow__square"></div>
					<input type="hidden" checked="checked" name="id_cuadrante" id="<?php echo 'id_cuadrante'.$k;?>" value="<?php echo $insert[$k]->id;?>">
					<input type="hidden" name="cont" id="cont" value="<?php echo $datos[0]->nombre;?>">
					<select name="paleta" id="<?php echo 'paleta'.$k;?>" class="form-control" style="display: inline-block; width: 65px">
						<?php 
						for ($j=1; $j <= 24 ; $j++) 
						{ 
							?>
							<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<?php
			}
		}
		?>
	</div>
	<?php 
	if (empty($datos)) 
	{ 
		?>
		<div class="col-lg-12" align="center" id="confPaleta">
			<a class="btn btn-primary" onclick="guardarConfiPaleta('<?php echo base_url(); ?>','<?php echo $cantCuadrante;?>',0)">PROCESAR</a>			
		</div>
		<div class="col-lg-12" align="center" id="confPaletaU" style="display: none;">
			<a class="btn btn-primary" onclick="guardarConfiPaleta('<?php echo base_url(); ?>','<?php echo $cantCuadrante;?>',1)">PROCESAR</a>
		</div>
		<?php	
	}
	else
	{
		?>
		<div class="col-lg-12" align="center">
			<a class="btn btn-primary" onclick="guardarConfiPaletaU('<?php echo base_url(); ?>','<?php echo $cantCuadrante;?>',2)">PROCESAR</a>
		</div>
		<?php 
	}
	?>