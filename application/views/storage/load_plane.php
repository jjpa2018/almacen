<style type="text/css">
.none
{
  display: none;
} 
</style>
<section class="panel">
  <div class="panel-body">
    <div id="post-info" style="text-align: center;">
      <div class="section__title">
        <h4 class="modal-title"><strong>CARGAR PLANO DEL SUB-ALMACEN : <div id="nameSubalmacen"></div></strong></h4>
      </div>         
      <div class="col-md-12">
        <label><strong>Para habilitar la cargar del plano verifique las caracteristicas requeridas por el sistema</strong></label>
      </div>
      <div class="col-md-12">
        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#caracteristicas" onclick="habilita_texto();"><strong>Caracteristicas
        para cargar plano</strong></button>
      </div>
      <p style="padding-top: 5%;"></p>
      <div id="info" class="none">
        <h4><strong>CARACTERISTICAS DEL PLANO REQUERIDAS POR EL
        SISTEMA</strong></h4>
        <p class="toUpper"><i class="fa fa-arrow-right"></i>la escala del pano debe ser</p>
        <p class="toUpper"><i class="fa fa-arrow-right"></i>la imagen debe tener alguno de los siguientes formatos: bmp,png, jpg</p>
        <p class="toUpper"><i class="fa fa-arrow-right"></i>el archivo de imagen debe tener maxicmo 5mb</p>
        <br>
        <p class="toUpper"><i class="fa fa-arrow-right"></i>todos los panos deben poseer las nomenclatura numerica de los cuandrantes en
        un orden logico correlativo siguiendo el sentido de las agujas del reloj</p>
        <br>
        <p class="toUpper"><i class="fa fa-arrow-right"></i>si un cuadrante possee varias paletas como en el ejemplo 11-a, 11-b, 11-c o
        11-d, se debe especificar con letras las paletas</p>

        <br>
        <p class="toUpper"><i class="fa fa-arrow-right"></i>en las areas centrales tambien se deben codificar nombrando los cuadrantes y
        las paletas o rack que lo componen</p>

        <br>
        <p class="toUpper"><i class="fa fa-arrow-right"></i>todos los estantes deben estar enumerados al igual que los cuadrantes</p>
        <a class="btn btn-info" onclick="ok();">Entendido</a>
      </div>          
    </div>
    <p style="padding-top: 5%;"></p>
    <form method="post" id="frmimagen" enctype="multipart/form-data">
      <div id="loadImage" class="none">
        <div class="col-lg-offset-5 col-lg-12">
          <div align="center" class="col-lg-12">
            <div class="fileupload fileupload-new" data-provides="fileupload">
              <div class="fileupload-new thumbnail" style="width: 400px; height: 400px;">
                <img src="<?=base_url().'img/no-image.png'?>" style="width: 400px; height: 400px; background:  #ebedef ;" alt="" />
              </div>
              <div class="fileupload-preview fileupload-exists thumbnail" style="width: 400px; height: 450px; line-height: 20px;"></div>
              <span class="btn btn-primary btn-file">
                <span class="fileupload-new" id="load_file">Seleccionar Archivo</span>
                <span class="fileupload-exists"><i class="fa fa-undo"></i> Cambiar</span>
                <input type="file" name="image_file" id="image_file" required /> 
              </span>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="alert alert-info fade in">
            <h4>Detalles del plano:</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <div class="square yellow__square"></div>
            <label><strong>Cuadrantes</strong></label>
          </div>
          <div class="col-md-4">
            <div class="square orange__square"></div>
            <label><strong>Estantes Metalicos</strong></label>
          </div>
          <div class="col-md-5">
            <div class="square gray__square"></div>
            <label><strong>Area de circulacion</strong></label>
          </div>
          <hr>
          <div class="col-md-1 mt-3">
            <div class="square yellow__square"></div>
          </div>
          <div class="col-md-11 mt-3">
            <P class=""><strong>CUADRANTES: ZONA DELIMINATA O DELIMITADA DENTRO DEL ALMACEN QUE POSSE UNA O
            VARIAS PALETAS DE ALMACENAMIENTO. EN LOS CUADRANTES PUEDEN ESTAR ALMECENADOS UNO O VARIOS PRODUCTOS</strong></P>
          </div>
          <div class="col-md-1 mt-3">
            <div class="square orange__square"></div>
          </div>
          <div class="col-md-11 mt-3">
            <P class=""><strong>ESTANTES METALICOS: ESTAN COMPUESTOS POR FILAS Y COLUMNAS Y DIVIDIDOS EN
              ENTREPANOS. AL IGUAL QUE EN LOS ESTANTES SE PUEDEN ALMACENAR UNO O VARIOS PRODUCTOS EN UN MISMO
            ENTREPANO</strong></P> 
          </div>
          <div class="col-md-1 mt-3">
            <div class="square gray__square"></div>
          </div>
          <div class="col-md-11 mt-3">
            <P class=""><strong>AREA DE CIRCULACION: LUGAR O AREA DONDE NO SE PUEDEN ALMACENAR PRODUCTOS, PARA
            NO OBSTRACULIZAR EL ACCESO A LOS CUADRANTES Y ESTANTES</strong></P>
          </div>
        </div>
        <div class="panel-body">
          <div align="right"  class="col-lg-12">
            <input type="submit" class="btn btn-success" name="upload_image" id="upload_image" value="CARGAR">
            <a class="btn btn-secondary" onclick="registro('<?php echo base_url(); ?>',3);">SIGUIENTE</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    $('#frmimagen').on('submit',function(e){
      e.preventDefault();
      if ($('#image_file').val() == '') 
      {
        alert("Por favor debe seleccionar una Imagen");
      }
      else
      {
        'use strict'
        var data = new FormData($("#frmimagen")[0]);
        var pruebaIdSA=$('#pruebaIdSA').val();
        $.ajax({
          type:'POST',
          url:'<?php echo base_url();?>index.php/upload/uploadimg/'+pruebaIdSA,
          data:data,
          contentType:false,
          cache:false,
          processData:false,
          success:function(data)
          {
            if (data == 1) 
            {
              alert("La imagen ha sido guardada con exito");
            }
            else
            {
              alert("Ocurrio un error al guardar la imagen");
            }
            
          }
        });
      }
    });
  });

</script>