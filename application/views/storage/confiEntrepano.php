<div class="row">
	<div class="col-md-12" align="center">
		<label class="toUpper">ESPECIFIQUE EL NUMERO DE ENTREPAÑO POR ESTANTE</label>
	</div>
	<?php 
	if (empty($datos)) 
	{

		for ($i=0; $i < $cantEstante; $i++) 
			{ 
				?>
				<div class="col-md-3"> 
					<label><strong><?php echo $i+1; ?></strong></label>
					<div class="square orange__square"></div>
					<input type="hidden" checked="checked" name="id_estante" id="<?php echo 'id_estante'.$i;?>" value="<?php echo $insert[$i]->id;?>">
					<select name="entrepano" id="<?php echo 'entrepano'.$i;?>" class="form-control" style="display: inline-block; width: 65px">
						<?php 
						for ($j=1; $j <= 24 ; $j++) 
						{ 
							?>
							<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<?php
			}
		}
		else
		{
			$cont=($datos[0]->nombre);
			for ($i=$cont; $i < $cantEstante; $i++) 
			{ 
				$k=$i-$cont;
				?>
				<div class="col-md-3">
					<label><strong><?php echo $i+1; ?></strong></label>
					<div class="square orange__square"></div>
					<input type="hidden" checked="checked" name="id_estante" id="<?php echo 'id_estante'.$k;?>" value="<?php echo $insert[$k]->id;?>">
					<input type="hidden" name="cont" id="cont" value="<?php echo $datos[0]->nombre;?>">
					<select name="entrepano" id="<?php echo 'entrepano'.$k;?>" class="form-control" style="display: inline-block; width: 65px">
						<?php 
						for ($j=1; $j <= 24 ; $j++) 
						{ 
							?>
							<option value="<?php echo $j; ?>"><?php echo $j; ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<?php
			}
		}
		?>
	</div>
	<?php 
	if (empty($datos)) 
	{ 
		?>
		<div class="col-lg-12" align="center" id="confEntrepano">
			<a class="btn btn-primary" onclick="guardarConfiEntrepano('<?php echo base_url(); ?>','<?php echo $cantEstante;?>',0)">PROCESAR</a>			
		</div>
		<div class="col-lg-12" align="center" id="confEntrepanoU">
			<a class="btn btn-primary" onclick="guardarConfiEntrepano('<?php echo base_url(); ?>','<?php echo $cantEstante;?>',1)">PROCESAR</a>
		</div>
		<?php	
	}
	else
	{
		?>
		<div class="col-lg-12" align="center">
			<a class="btn btn-primary" onclick="guardarConfiEntrepanoU('<?php echo base_url(); ?>','<?php echo $cantEstante;?>',2)">PROCESAR</a>
		</div>
		<?php 
	}
	?>