<?php 
foreach ($datosAlmacen as $key) 
{ 
	$region=$key['7'];
	$estado=$key['8'];
	$tipo_sede=$key['6'];
	$sede=$key['2'];

} 
?>
<div class="row">
<input type="hidden" id="imagen" value="<?php echo $idu?>">
<div class="col-lg-12">
	<br>
	<h3><font size="4" color="#001C67">PARAMETRIZACIÓN DE ALMACENES</font></h3>
	<hr>
</div>
<div class="col-lg-12">
	<div class="section__title" style="margin-top: 1rem">
		<h4>Ubicación del almacén</h4>
	</div>
</div>
<p style="padding-top: 5%;"></p>
	<div class="form-group">
		<div class="col-lg-12">
			<label class="labels">Región</label>
			<input type="text" class="form-control form-control-sm" readonly name="" id="" value="<?php echo $region; ?>">
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-12">
			<label class="labels">Estado</label>
			<input type="text" class="form-control form-control-sm" readonly name="" id="" value="<?php echo $estado; ?>">
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-12">
			<label class="labels">Tipo de Sede</label>
			<input type="text" class="form-control form-control-sm" readonly name="" id="t_h" value="<?php echo $tipo_sede; ?>">
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-12">
			<label class="labels">Sede</label>
			<input type="text" class="form-control form-control-sm" readonly name="" id="h" value="<?php echo $sede; ?>">
		</div>
	</div>
<p style="padding-top: 1%;"></p>

	<div class="col-lg-12">
		<div class="section__title" style="margin-top: 1rem">
			<h4>Caracteristicas del almacen / sub almacen</h4>
		</div>
	</div>
	<div class="col-lg-12">
		<div id="almacenes">

		</div>
		<div id="subalmacenes">

		</div>
	

<input type="hidden" name="idu" id="idu" value="<?php echo $idu?>">

<div class="modal bd-example-modal-md closeM " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="subAlmacen1">
	<div class="modal-dialog modal-dialog-centered modal-md"  role="document">
		<div class="modal-content mdcontent">
			<div id="loadView">

			</div>
		</div>
	</div>
</div>
