<?php 

  $foto=$imgSA[0][0];
 ?>
<section class="panel">
  <div class="panel-body">
    <p style="padding-top: 5%;"></p>
    <form method="post" id="frmimagenU" enctype="multipart/form-data">
      <div id="loadImage" class="none">
        <div class="col-lg-offset-5 col-lg-12">
          <div align="center" class="col-lg-12">
            <div class="fileupload fileupload-new" data-provides="fileupload">
              <div class="fileupload-new thumbnail" style="width: 400px; height: 400px;">
                <img src="<?php echo $foto;?>" style="width: 400px; height: 400px; background:  #ebedef ;" alt="" />
              </div>
              <div class="fileupload-preview fileupload-exists thumbnail" style="width: 400px; height: 450px; line-height: 20px;"></div>
              <span class="btn btn-primary btn-file">
                <span class="fileupload-new" id="load_file">Seleccionar Archivo</span>
                <span class="fileupload-exists"><i class="fa fa-undo"></i> Cambiar</span>
                <input type="file" name="image_file" id="image_file" value="<?php echo $foto;?>" required /> 
              </span>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="alert alert-info fade in">
            <h4>Detalles del plano:</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <div class="square yellow__square"></div>
            <label><strong>Cuadrantes</strong></label>
          </div>
          <div class="col-md-4">
            <div class="square orange__square"></div>
            <label><strong>Estantes Metalicos</strong></label>
          </div>
          <div class="col-md-5">
            <div class="square gray__square"></div>
            <label><strong>Area de circulacion</strong></label>
          </div>
          <hr>
          <div class="col-md-1 mt-3">
            <div class="square yellow__square"></div>
          </div>
          <div class="col-md-11 mt-3">
            <P class=""><strong>CUADRANTES: ZONA DELIMINATA O DELIMITADA DENTRO DEL ALMACEN QUE POSSE UNA O
            VARIAS PALETAS DE ALMACENAMIENTO. EN LOS CUADRANTES PUEDEN ESTAR ALMECENADOS UNO O VARIOS PRODUCTOS</strong></P>
          </div>
          <div class="col-md-1 mt-3">
            <div class="square orange__square"></div>
          </div>
          <div class="col-md-11 mt-3">
            <P class=""><strong>ESTANTES METALICOS: ESTAN COMPUESTOS POR FILAS Y COLUMNAS Y DIVIDIDOS EN
              ENTREPANOS. AL IGUAL QUE EN LOS ESTANTES SE PUEDEN ALMACENAR UNO O VARIOS PRODUCTOS EN UN MISMO
            ENTREPANO</strong></P> 
          </div>
          <div class="col-md-1 mt-3">
            <div class="square gray__square"></div>
          </div>
          <div class="col-md-11 mt-3">
            <P class=""><strong>AREA DE CIRCULACION: LUGAR O AREA DONDE NO SE PUEDEN ALMACENAR PRODUCTOS, PARA
            NO OBSTRACULIZAR EL ACCESO A LOS CUADRANTES Y ESTANTES</strong></P>
          </div>
        </div>
        <div class="panel-body">
          <div align="right"  class="col-lg-12">
            <input type="submit" class="btn btn-success" name="upload_image" id="upload_image" value="CARGAR">
             <button class="btn btn-secondary" onclick="registro('<?php echo base_url(); ?>',3);">SIGUIENTE</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script>
  $(document).ready(function(){
    $('#frmimagenU').on('submit',function(e){
      e.preventDefault();
      if ($('#image_file').val() == '') 
      {
        alert("Por favor debe seleccionar una Imagen");
      }
      else
      {
        'use strict'
        var data = new FormData($("#frmimagenU")[0]);
        var pruebaIdSA=$('#pruebaIdSA').val();
        $.ajax({
          type:'POST',
          url:'<?php echo base_url();?>index.php/upload/uploadimgU/'+pruebaIdSA,
          data:data,
          contentType:false,
          cache:false,
          processData:false,
          success:function(data)
          {
            if (data == 1) 
            {
              alert("La imagen ha sido guardada con exito");
            }
            else
            {
              alert("Ocurrio un error al guardar la imagen");
            }
            
          }
        });
      }
    });
  });

</script>