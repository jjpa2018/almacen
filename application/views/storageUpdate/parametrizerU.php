<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="section__title">
					configuracion del sub almacen: <p id="name"></p>
				</div>
				<br>
				<div align="center" class="col-lg-12">
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: 400px; height: 400px;">
							<?php 
							if (!empty($imgSA)) 
								{ ?>
									<img src="<?php echo $imgSA[0][0]?>" style="width: 400px; height: 400px; background:  #ebedef ;" alt="" />
									<?php 
								}
								else
								{
									?>
									<img src="<?=base_url().'img/no-image.png'?>" style="width: 400px; height: 400px; background:  #ebedef ;" alt="" />
									<div class="alert alert-danger alert-dismissible fade show" align="center" role="alert">
										<strong>No existe ninguna Imagén para el sub-almacen</strong>
									</div>
									<?php 
								} ?>

							</div>
						</div>
					</div>
					<div class="section__title" style="margin-top: 1rem">
						CONFIGURACION DE CUADRANTES
					</div>
					<?php 
					if ($datosCuadrantes != 0 || $cantCuadrantes != 0) 
					{
						?>
						<br>	        	
						<div class="dataTable_wrapper">
							<div class="table-responsive col-sm-12">
								<div class="btn-group-sm" role="group" aria-label="Basic example" style="float: right; padding: 0% 0% 1% 0%;">
									<button type="button" class="btn btn-info" onclick="create_cuadrante('<?php echo base_url();?>',1);">CREAR CUADRANTE</button>
									<button type="button" class="btn btn-info" onclick="create_paleta('<?php echo base_url();?>',2);">CREAR PALETA</button>
								</div>
								<table class="dt table table-sm table-striped table-hover table-bordered" id="tablaSM">
									<thead style="text-transform: uppercase;" class="theadH">
										<tr>
											<th width="5%"><strong>N°</strong></th>
											<th><strong>Cuadrante</strong></th>
											<th>Acciones</th>	
										</tr>
									</thead> 
									<tbody>

										<?php  
										if ($cantCuadrantes != 0) 
										{
											$nro = 1;
											foreach ($cantCuadrantes as $key) 
											{ 
												$id=$key['0'];
												$cuadrante=$key['1'];
												?>

												<tr>
													<td align="center"><strong><?php echo $nro; ?></strong></td>
													<td align="center"><strong><?php echo $cuadrante; ?></strong></td>
													<td align="center">
														<h3><button class="btn btn-outline-danger btn-sm" onclick="delet('<?php echo base_url(); ?>',99,'<?php echo $id; ?>','<?php echo $id; ?>');"><i class="fa fa-trash"></i></button></h3>
													</td>
													<input type="hidden" name="id_cuadrante" id="id_cuadrante" value="<?php echo $id; ?>">
												</tr>
												<?php $nro++;  
											} 
										}
										else
											{ ?>
												<tr>
													<td colspan="3"> 
														<div class="alert alert-danger alert-dismissible fade show" role="alert">
															<strong>No existen Cuadrantes configurados en este sub-almacen</strong>
															<button class="btn btn-secondary" onclick="create_cuadrante('<?php echo base_url();?>',1);">CREAR CUADRANTE</button>
														</div>
													</td>
												</tr>
											<?php	}
											?>
										</tbody>
									</table>

									<?php 
									$nro = 1;
									if ($datosCuadrantes != 0) 
									{
										?>
										
										<table class="dt table table-sm table-striped table-hover table-bordered" id="tablaSM">
											<thead style="text-transform: uppercase;" class="theadH">
												<tr>
													<th width="5%"><strong>N°</strong></th>
													<th><strong>Cuadrante</strong></th>
													<th><strong>Paleta</strong></th>
													<th>Acciones</th>	
												</tr>
											</thead>
											<tbody>
												<?php
												foreach ($datosCuadrantes as $key) 
												{ 
													$id=$key['2'];
													$cuadrante=$key['1'];
													$paleta=$key['3'];
													$id_cuadrante=$key['0'];
													?>
													<tr>
														<td align="center"><strong><?php echo $nro; ?></strong></td>
														<td align="center"><strong><?php echo $cuadrante; ?></strong></td>
														<td align="center"><strong><?php echo $paleta; ?></strong></td>
														<td align="center">
															<h3><button class="btn btn-outline-danger btn-sm" onclick="delet('<?php echo base_url(); ?>',1,'<?php echo $id_cuadrante; ?>','<?php echo $id; ?>');"><i class="fa fa-trash"></i></button></h3>
														</td>
														<input type="hidden" name="id<?php echo $nro;?>" id="id<?php echo $nro;?>" value="<?php echo $id; ?>">
													</tr>
													<?php $nro++;  
												}
											}
											else
												{ ?>
													<tr>
														<td colspan="3"> 
															<br>
															<div class="alert alert-danger alert-dismissible fade show" role="alert" id="msj_err_c">
																<strong>No existen Cuadrantes configurados en este sub-almacen</strong>
																<button class="btn btn-secondary" onclick="create_cuadrante('<?php echo base_url();?>',1);">CREAR CUADRANTE</button>
															</div>
														</td>
													</tr>
												<?php	}
											}
											else
												{ ?>
													<tr>
														<td colspan="3"> 
															<br>
															<div class="alert alert-danger alert-dismissible fade show" role="alert" id="msj_err_c">
																<strong>No existen Cuadrantes configurados en este sub-almacen</strong>
																<button class="btn btn-secondary" onclick="create_cuadrante('<?php echo base_url();?>',1);">CREAR CUADRANTE</button>
															</div>
														</td>
													</tr>
												<?php	}
												?>
											</tbody>
										</table>
										<div id="create_cuadrante">

										</div>
									</div>
								</div>

								<div class="section__title" style="margin-top: 1rem">
									CONFIGURACION DE ESTANTES
								</div>
								<?php 
								if ($datosEstantes != 0 || $cantEstantes != 0) 
									{ ?>
										<br>	        	
										<div class="dataTable_wrapper">
											<div class="table-responsive col-sm-12">
												<div class="btn-group-sm" role="group" aria-label="Basic example" style="float: right; padding: 0% 0% 1% 0%;">
													<button type="button" class="btn btn-info" onclick="create_estante('<?php echo base_url();?>',3);">CREAR ESTANTE</button>
													<button type="button" class="btn btn-info" onclick="create_entrepano('<?php echo base_url();?>',4);">CREAR ENTREPAÑO</button>
												</div>
												<?php 
												if ($cantEstantes != 0) 
													{ ?>
														<table class="dt table table-sm table-striped table-hover table-bordered" id="tablaSM">
															<thead style="text-transform: uppercase;" class="theadH">
																<tr>
																	<th width="5%"><strong>N°</strong></th>
																	<th><strong>Estante</strong></th>
																	<th>Acciones</th>	
																</tr>
															</thead>
															<tbody>

																<?php 

																$nro = 1;
																$cantidad=count($datosEstantes);
																foreach ($cantEstantes as $key) 
																{ 
																	$id=$key['0'];
																	$estante=$key['1'];
																	?>

																	<tr>
																		<td align="center"><strong><?php echo $nro; ?></strong></td>
																		<td align="center"><strong><?php echo $estante; ?></strong></td>
																		<td align="center">
																			<h3><button class="btn btn-outline-danger btn-sm" onclick="delet('<?php echo base_url(); ?>',98,'<?php echo $id; ?>','<?php echo $id; ?>');"><i class="fa fa-trash"></i></button></h3>
																		</td>
																		<input type="hidden" name="id<?php echo $nro;?>" id="id<?php echo $nro;?>" value="<?php echo $id; ?>">
																	</tr>
																	<?php $nro++;  
																}
															} 
															else
																{ ?>
																	<tr>
																		<td colspan="3"> 
																			<div class="alert alert-danger alert-dismissible fade show" role="alert" id="msj_err_e">
																				<strong>No existen Estantes configurados en este sub-almacen</strong>
																				<button class="btn btn-secondary" onclick="create_estante('<?php echo base_url();?>',3);">CREAR ESTANTE</button>
																			</div>
																		</td>
																	</tr>
																<?php	}
																?>
															</tbody>
														</table>
														<?php 
														if ($datosEstantes != 0) 
															{ ?>
																<table class="dt table table-sm table-striped table-hover table-bordered" id="tablaSM">
																	<thead style="text-transform: uppercase;" class="theadH">
																		<tr>
																			<th width="5%"><strong>N°</strong></th>
																			<th><strong>Estante</strong></th>
																			<th><strong>Entrepaño</strong></th>
																			<th>Acciones</th>	
																		</tr>
																	</thead>
																	<tbody>

																		<?php 

																		$nro = 1;
																		$cantidad=count($datosEstantes);
																		foreach ($datosEstantes as $key) 
																		{ 
																			$id=$key['2'];
																			$estante=$key['1'];
																			$entrepano=$key['3'];
																			$id_estante=$key['0'];
																			?>

																			<tr>
																				<td align="center"><strong><?php echo $nro; ?></strong></td>
																				<td align="center"><strong><?php echo $estante; ?></strong></td>
																				<td align="center"><strong><?php echo $entrepano; ?></strong></td>
																				<td align="center">
																					<h3><button class="btn btn-outline-danger btn-sm" onclick="delet('<?php echo base_url(); ?>',2,'<?php echo $id_estante; ?>','<?php echo $id; ?>');"><i class="fa fa-trash"></i></button></h3>
																				</td>
																				<input type="hidden" name="id<?php echo $nro;?>" id="id<?php echo $nro;?>" value="<?php echo $id; ?>">
																			</tr>
																		</tr>
																		<?php $nro++;  
																	}
																} 
																else
																	{ ?>
																		<tr>
																			<td colspan="3"> 
																				<div class="alert alert-danger alert-dismissible fade show" role="alert" id="msj_err_e">
																					<strong>No existen Estantes configurados en este sub-almacen</strong>
																					<button class="btn btn-secondary" onclick="create_estante('<?php echo base_url();?>',3);">CREAR ESTANTE</button>
																				</div>
																			</td>
																		</tr>
																	<?php	}
																}
																else
																	{ ?>
																		<tr>
																			<td colspan="3"> 
																				<div class="alert alert-danger alert-dismissible fade show" role="alert" id="msj_err_e">
																					<strong>No existen Estantes configurados en este sub-almacen</strong>
																					<button class="btn btn-secondary" onclick="create_estante('<?php echo base_url();?>',3);">CREAR ESTANTE</button>
																				</div>
																			</td>
																		</tr>
																	<?php	}
																	?>
																</tbody>
															</table>
															<div id="create_estante">

															</div>
														</div>
													</div>

													<div class="section__title" style="margin-top: 1rem">
														CONFIGURACION DE RACK
													</div>

													<br>	        	
													<div class="dataTable_wrapper">
														<div class="table-responsive col-sm-12">
															<?php 	
															if ($datosRack != 0) 
																{ ?>
																	<div class="btn-group-sm" role="group" aria-label="Basic example" style="float: right; padding: 0% 0% 1% 0%;">
																		<button type="button" class="btn btn-info" onclick="create_rack('<?php echo base_url();?>',5);">CREAR RACK</button>
																		<button type="button" class="btn btn-info" onclick="create_fila('<?php echo base_url();?>',6);">CREAR FILA</button>
																		<button type="button" class="btn btn-info" onclick="create_columna('<?php echo base_url();?>',7);">AGREGAR COLUMNA</button>
																	</div>
																	<table class="dt table table-sm table-striped table-hover table-bordered" id="tablaSM">
																		<thead style="text-transform: uppercase;" class="theadH">
																			<tr> 
																				<th width="5%"><strong>N°</strong></th>
																				<th><strong>Rack</strong></th>
																				<th><strong>Fila -a Columna</strong></th>
																				<th>Acciones</th>	
																			</tr>
																		</thead>
																		<tbody>

																			<?php 
																			$nro = 1;
																			$cantidad=count($datosRack);
																			foreach ($datosRack as $key) 
																			{ 
																				$id=$key['2'];
																				$rack=$key['1'];
																				$espacios=$key['3'];
																				$id_espacioRack=$key['0'];
																				?>

																				<tr>
																					<td align="center"><strong><?php echo $nro; ?></strong></td>
																					<td align="center"><strong><?php echo $rack; ?></strong></td>
																					<td align="center"><strong><?php echo $espacios; ?></strong></td>
																					<td align="center">
																						<h3><button class="btn btn-outline-danger btn-sm" onclick="delet('<?php echo base_url(); ?>',3,'<?php echo $id_espacioRack; ?>','<?php echo $id; ?>');"><i class="fa fa-trash"></i></button></h3>
																					</td>
																					<input type="hidden" name="id<?php echo $nro;?>" id="id<?php echo $nro;?>" value="<?php echo $id; ?>">
																				</tr>
																				<?php $nro++;  
																			} 
																		} 
																		else
																			{ ?>
																				<tr>
																					<td colspan="3"> 
																						<div class="alert alert-danger alert-dismissible fade show" role="alert" id="msj_err_r">
																							<strong>No existen Racks configurados en este sub-almacen</strong>
																							<button class="btn btn-secondary" onclick="create_rack('<?php echo base_url();?>',5);">CREAR RACK</button>
																						</div>
																					</td>
																				</tr>
																				<?php	
																			}
																			?>
																		</tbody>
																	</table>
																	<div id="create_rack">

																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>