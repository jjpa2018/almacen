<div class="section__title">
	CREAR CCOLUMNAS DEL RACK <p class="toUpper"></p>
</div>
<div class="row" style="padding-top: 2%;">
	<div align="center" class="col-lg-12">
		<label><strong>RACKS:</strong></label>
		<div class="col-xs-1" style="display: inline-block">
			<select name="id_rack" id="id_rack" class="form-control" onchange="load_nameCR('<?php echo base_url(); ?>');">
				<option>SELECCIONE:</option>
				<?php 
				$contar=count($datos);

				for ($i=0; $i < $contar ; $i++) 
				{
					?>
					<option value="<?php echo $datos[$i]->id; ?>"><?php echo $datos[$i]->nombre; ?></option>
					<?php
				}
				?>
			</select>

		</div>
	</div>

	<div class="col-md-12" align="center" style="padding-top: 2%;">

		<div class="control-panel">
			<div class="form-group">
				<div id="ver_info" style="">
					<label><strong>FILAS:</strong></label>
					<div class="col-xs-6" style="display: inline-block" >
						<select name="id_fila" id="id_fila" class="form-control" onchange="loadColumn('<?php echo base_url();?>');">
							<option>SELECCIONE...</option>
						</select>
					</div>
					<label><strong>COLUMNAS:</strong></label>
					<div class="col-xs-6" style="display: inline-block">
						<div id="pruC"></div>
					</div>

				</div>
			</div>
		</div>
	</div> 
</div>
<br>
<div class="col-lg-12" align="center">
	<a class="btn btn-primary" onclick="guardarConfiRacUII('<?php echo base_url(); ?>');">PROCESAR</a>
</div>