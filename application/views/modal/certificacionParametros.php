<div class="modal-header">
	<h4 class="modal-title"><strong>Detalles del Sub-Almacen</strong></h4>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<div class="modal-body">
	<div class="section__title">
		<h4>CERTIFICACION DE PARAMETROS CONFIGURADOS</h4>
	</div>
	<?php 
	if (!empty($datosAlmacen) ) {

		foreach ($datosAlmacen as $key) 
		{ 
			$region=$key['7'];
			$estado=$key['8'];
			$tipo_sede=$key['6'];
			$sede=$key['2'];
			$nombre=$key['10'];
		}

		?>
		<div class="row">
			<div class="col-lg-12">
				<div class="section__title" style="margin-top: 1rem">
					<h4>Ubicación del almacén</h4>
				</div>
			</div>
			<p style="padding-top: 5%;"></p>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="col-lg-12">
						<label class="labels">Región</label>
						<input type="text" class="form-control form-control-sm" readonly name="" id="" value="<?php echo $region; ?>">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="col-lg-12">
						<label class="labels">Estado</label>
						<input type="text" class="form-control form-control-sm" readonly name="" id="" value="<?php echo $estado; ?>">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="col-lg-12">
						<label class="labels">Tipo de Sede</label>
						<input type="text" class="form-control form-control-sm" readonly name="" id="t_h" value="<?php echo $tipo_sede; ?>">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="col-lg-12">
						<label class="labels">Sede</label>
						<input type="text" class="form-control form-control-sm" readonly name="" id="h" value="<?php echo $sede; ?>">
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<div class="col-lg-12">
						<label class="labels">Sub Almacen</label>
						<input type="text" class="form-control form-control-sm" readonly name="" id="h" value="<?php echo $nombre; ?>">
					</div>
				</div>
			</div>
		</div>
		<p style="padding-top: 1%;"></p>
		<div class="section__title" style="margin-top: 1rem">
			PLANO DEL SUB ALMACEN
		</div>

		<br>
		<div class="col-md-12">
			<div class="col-lg-offset-5 col-lg-12">
				<div align="center" class="col-lg-12">
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<div class="fileupload-new thumbnail" style="width: 400px; height: 400px;">
							<?php 
							if (!empty($imgSA)) 
								{ ?>
									<img src="<?php echo $imgSA[0][0];?>" style="width: 400px; height: 400px; background:  #ebedef ;" alt="" />
									<?php 
								}
								else
								{
									?>
									<img src="<?=base_url().'img/no-image.png'?>" style="width: 400px; height: 400px; background:  #ebedef ;" alt="" />
									<div class="alert alert-danger alert-dismissible fade show" align="center" role="alert">
										<strong>No existe ninguna Imagén para el sub-almacen</strong>
									</div>
									<?php 
								} ?>

							</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="section__title" style="margin-top: 1rem">
				CONFIGURACION DE CUADRANTES
			</div>

			<br>	        	
			<div class="dataTable_wrapper">
				<div class="table-responsive col-sm-12">

					<table class="dt table table-sm table-striped table-hover table-bordered" id="tablaSM">
						<thead style="text-transform: uppercase;" class="theadH">
							<tr>
								<th width="5%"><strong>N°</strong></th>
								<th><strong>Cuadrante</strong></th>
								<th><strong>Paleta</strong></th>	
							</tr>
						</thead> 
						<tbody>

							<?php 
							if ($datosCuadrantes != 0) 
							{
								$nro = 1;
								foreach ($datosCuadrantes as $key) 
								{ 
									$id=$key['2'];
									$cuadrante=$key['1'];
									$paleta=$key['3'];
									$id_cuadrante=$key['0'];
									?>

									<tr>
										<td align="center"><strong><?php echo $nro; ?></strong></td>
										<td align="center"><strong><?php echo $cuadrante; ?></strong></td>
										<td align="center"><strong><?php echo $paleta; ?></strong></td>
										<input type="hidden" name="id_cuadrante" id="id_cuadrante" value="<?php echo $id_cuadrante; ?>">
									</tr>
									<?php $nro++;  
								} 
							}
							else
								{ ?>
									<tr>
										<td colspan="3"> 
											<div class="alert alert-danger alert-dismissible fade show" role="alert">
												<strong>No existen Cuadrantes configurados en este sub-almacen</strong>
											</div>
										</td>
									</tr>
								<?php	}
								?>
							</tbody>
						</table>
					</div>
				</div>

				<div class="section__title" style="margin-top: 1rem">
					CONFIGURACION DE ESTANTES
				</div>

				<br>	        	
				<div class="dataTable_wrapper">
					<div class="table-responsive col-sm-12">

						<table class="dt table table-sm table-striped table-hover table-bordered" id="tablaSM">
							<thead style="text-transform: uppercase;" class="theadH">
								<tr>
									<th width="5%"><strong>N°</strong></th>
									<th><strong>Estante</strong></th>
									<th><strong>Entrepaño</strong></th>	
								</tr>
							</thead>
							<tbody>

								<?php 
								if ($datosEstantes != 0) 
								{
									$nro = 1;
									foreach ($datosEstantes as $key) 
									{ 
										$id=$key['2'];
										$estante=$key['1'];
										$entrepano=$key['3'];
										$id_estante=$key['0'];
										?>

										<tr>
											<td align="center"><strong><?php echo $nro; ?></strong></td>
											<td align="center"><strong><?php echo $estante; ?></strong></td>
											<td align="center"><strong><?php echo $entrepano; ?></strong></td>
											<input type="hidden" name="id_estante" id="id_estante" value="<?php echo $id_estante; ?>">
										</tr>
										<?php $nro++;  
									}
								}
								else
									{ ?>
										<tr>
											<td colspan="3"> 
												<div class="alert alert-danger alert-dismissible fade show" role="alert">
													<strong>No existen Estantes configurados en este sub-almacen</strong>
												</div>
											</td>
										</tr>
									<?php	}
									?>
								</tbody>
							</table>
						</div>
					</div>

					<div class="section__title" style="margin-top: 1rem">
						CONFIGURACION DE RACK
					</div>

					<br>	        	
					<div class="dataTable_wrapper">
						<div class="table-responsive col-sm-12">

							<table class="dt table table-sm table-striped table-hover table-bordered" id="tablaSM">
								<thead style="text-transform: uppercase;" class="theadH">
									<tr>
										<th width="5%"><strong>N°</strong></th>
										<th><strong>Rack</strong></th>
										<th><strong>Fila -a Columna</strong></th>	
									</tr>
								</thead>
								<tbody>

									<?php 
									if ($datosRack != 0) 
									{
										$nro = 1;
										foreach ($datosRack as $key) 
										{ 
											$id=$key['2'];
											$rack=$key['1'];
											$espacios=$key['3'];
											$id_espacioRack=$key['0'];
											?>

											<tr>
												<td align="center"><strong><?php echo $nro; ?></strong></td>
												<td align="center"><strong><?php echo $rack; ?></strong></td>
												<td align="center"><strong><?php echo $espacios; ?></strong></td>
												<input type="hidden" name="id_espacioRack" id="id_espacioRack" value="<?php echo $id_espacioRack; ?>">
											</tr>
											<?php $nro++;  
										}
									}
									else
										{ ?>
											<tr>
												<td colspan="3"> 
													<div class="alert alert-danger alert-dismissible fade show" role="alert">
														<strong>No existen Racks configurados en este sub-almacen</strong>
													</div>
												</td>
											</tr>
											<?php	
										}
										?>
									</tbody>
								</table>
							</div>
						</div>
						<?php 
					}
					else
						{ ?>
							<br><br>
							<div class="alert alert-danger alert-dismissible fade show" align="center" role="alert">
								<strong>No existen ninguna configuración registrada, favor verifique</strong>
							</div>
						<?php } ?>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">CERRAR</button>
					</div>