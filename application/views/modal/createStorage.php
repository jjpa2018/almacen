<div id="exampleModalLongTitle" class="modal-header">
	<div align="center" class="col-md-10">
        <h4 class="modal-title" id="exampleModalCenterTitle"><strong>CREAR ALMACEN</strong></h4>
    </div>
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<div class="modal-body">
	<div class="section__title">
		<h4 class="modal-title"><strong>DATOS DEL ALMACEN:</strong></h4>
	</div> 
	<div class="col-lg-12">
		<br><br>
		<div class="row">
			<div class=" form-inline">
				<label class="labels">NOMBRE DEL ALMACÉN:</label>
			</div>
			<div  class="col-md-12">
				<input type="text" class="form-control" name="nombre_almacen_new" id="nombre_almacen_new" placeholder="Ingrese el nombre del almacén a crear">
			</div>
		</div>
		<br>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
	<button class="btn btn-primary" onclick="guardar('<?php echo base_url();?>');">Guardar</button>
</div>






