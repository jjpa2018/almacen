<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	/** 
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->model('upload_model', '', TRUE);	
		$this->load->helper(array('url','form'));
	}

	/**
	 * esta funcion gestiona la carga de imagenes 
	 */
	public function uploadimg($pruebaIdSA) 
	{ 
		if(isset($_FILES["image_file"]["name"])) 
		{ 
			$imagen =$_FILES["image_file"]["name"];
			$pruebaIdSA=$pruebaIdSA;
			$extImage=explode(".", $imagen);// aqui pregunto que tipo de extencion tiene la imagen para validar que sea la permitida a cargar.
			if ($extImage[1] == 'jpg') 
			{
				$extImageI=$pruebaIdSA.".".$extImage[1];
			}
			elseif ($extImage[1] == 'png') 
			{
				$extImageI=$pruebaIdSA.".".$extImage[1];
			}
			elseif($extImage[1] == 'bmp')
			{
				$extImageI=$pruebaIdSA.".".$extImage[1]; 
			}

			if (isset($extImageI)) //pregunto si existe algun tipo de extension de las anteriormente validada
			{//si existe hace la insercion

				//configuraciones previas para la carga de imagen
				$config['upload_path'] = '../cocai-ivss/public/img/';//establezco la ruta a donde sera enviado el archivo
				$config['file_name'] = $extImageI;// establezco el nombre del archivo
				$config['allowed_types'] = 'jpg|png|bmp'; //  tipos de archivos permitidos
				$config['max_size'] = '5000';
				$config['max_width'] = '2024';
				$config['max_height'] = '2008'; 
				$this->load->library('upload', $config); 
				if(!$this->upload->do_upload('image_file')) //evalua si no existe un archivo para cargar y devuelve un mensaje de lo contrario realiza la carga
				{ 
					echo $this->upload->display_errors(); 
				} 
				else 
				{ 
					$ruta="http://localhost:88/solimcafer/cocai-ivss/public/img/".$extImageI;// la ruta que sera enviada al modelo para cargarlo en la base de datos 
					$insert=$this->upload_model->upload_image($pruebaIdSA,$ruta);
					echo json_encode($insert);
				} 
			}
			else
			{
				echo json_encode(0);
			}
			
		} 
	}

	public function uploadimgU($pruebaIdSA) 
	{ 
		if(isset($_FILES["image_file"]["name"])) 
		{ 
			$imagen =$_FILES["image_file"]["name"];
			$pruebaIdSA=$pruebaIdSA;
			$extImage=explode(".", $imagen);
			if ($extImage[1] == 'jpg') 
			{
				$extImageI=$pruebaIdSA.".".$extImage[1];
			}
			elseif ($extImage[1] == 'png') 
			{
				$extImageI=$pruebaIdSA.".".$extImage[1];
			}
			elseif($extImage[1] == 'bmp')
			{
				$extImageI=$pruebaIdSA.".".$extImage[1]; 
			}

			if (isset($extImageI)) 
			{
				$config['upload_path'] = '../cocai-ivss/public/img/';
				$config['file_name'] = $extImageI;
				$config['allowed_types'] = 'jpg|png|bmp';
				$config['max_size'] = '5000';
				$config['max_width'] = '2024';
				$config['max_height'] = '2008'; 
				$this->load->library('upload', $config); 
				if(!$this->upload->do_upload('image_file')) 
				{ 
					echo $this->upload->display_errors(); 
				} 
				else 
				{ 
					$ruta="http://localhost/jefer/cocai-ivss/public/img/".$extImageI;
					$insert=$this->upload_model->upload_imageU($pruebaIdSA,$ruta);
					echo json_encode($insert);
				} 
			}
			else
			{
				echo json_encode(0);
			}

			
		} 
	}

}
?>