<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Storage extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		$this->load->model('storage_model', '', TRUE);//cargo el modelo para usarlo mas adelante en las consultas
		$this->load->model('storageUpdate_model', '', TRUE);//cargo el modelo para usarlo mas adelante en las consultas
		$this->load->helper(array('url','form')); //cargo los helpers
	} 

	/**
	 * la funcion main recibe como parametro $tipo que indica que vista va a cargar al momento de crear un subalmacen
	 *tipos:
	 *1) crear subalmacen
	 *2) cargar imagen
	 *3) Parametrizacion de subalmacen
	 *4) Certificacion de parametros
	 */

	public function main($tipo)
	{
		if ($tipo == 1) 
		{
			$pruebaIdSA=$this->input->post('pruebaIdSA');// capturo el valor del subalmacen
			if (empty($pruebaIdSA)) //pregunto si la variable tiene algun dato (existe)
			{ // no existe o no tiene nada esa variable cargo la vista de creacion de subalmacen
				$almacen=$this->input->post('almacen');
				$data=array('almacen' => $almacen);
				$this->load->view('storage/create_storage',$data);
			}
			else
			{//si existe cambio la vista por la vista de edicion de subalmacenes
				$almacen=$this->input->post('almacen');
				$dataSA=$this->storageUpdate_model->datSA($pruebaIdSA);
				$data=array('almacen' => $almacen,'dataSA'=>$dataSA);
				$this->load->view('storageUpdate/create_storageU',$data);
			}
			
		}
		elseif ($tipo == 2) 
		{
			$pruebaIdSA=$this->input->post('pruebaIdSA');// capturo el valor del subalmacen
			$imgSA=$this->storageUpdate_model->imgSA($pruebaIdSA);// consulto si ya existe una imagen almacenada
			if ($imgSA[0][0] == NULL) //pregunto si de la consulta realizada trae algun valor
			{// si no hay valor alamacenada, muestro la vista de carga de imagen

				$this->load->view('storage/load_plane');		
			}
			else
			{// si hay imagen cargada muestro la vista de actualizacion de imagen

				$imgSA=$this->storageUpdate_model->imgSA($pruebaIdSA);
				$data=array('imgSA'=>$imgSA);
				$this->load->view('storageUpdate/load_planeU',$data);
			}
			
		}
		elseif ($tipo == 3) 
		{
			$pruebaIdSA=$this->input->post('pruebaIdSA');// capturo el valor del subalmacen
			
			if (empty($pruebaIdSA)) //pregunto si la variable tiene algun dato (existe)
			{// no existe o no tiene nada esa variable cargo la vista de parametrizacion de almacenes
				$this->load->view('storage/parametrizer');
			}
			else
			{
				$almacen=$this->input->post('almacen');
				$dataSA=$this->storageUpdate_model->datSA($pruebaIdSA);
				$cantCuadrantes=$this->storageUpdate_model->cuadrantesExistentes($pruebaIdSA);
				$cantEstantes=$this->storageUpdate_model->estantesExistentes($pruebaIdSA);
				$datosCuadrantes=$this->storage_model->certificacionCuadrantes($pruebaIdSA);
				$datosEstantes=$this->storage_model->certificacionEstantes($pruebaIdSA);
				$datosRack=$this->storage_model->certificacionRack($pruebaIdSA);
				$imgSA=$this->storageUpdate_model->imgSAp($pruebaIdSA);
				if (($datosCuadrantes == 0) and ($datosEstantes == 0) and ($datosRack == 0)) 
				{
					$data=array('imgSA'=>$imgSA);
					$this->load->view('storage/parametrizer',$data);
				}
				else
				{//si existe cambio la vista por la vista de edicion de parametrizacion de almacenes y le paso por un arreglo los  valores que se encuentran almacenados en la base de datos para ese subalmacen.
					$imgSA=$this->storageUpdate_model->imgSA($pruebaIdSA);
					$data=array('datosCuadrantes'=>$datosCuadrantes,'datosEstantes'=>$datosEstantes,'datosRack'=>$datosRack,'almacen' => $almacen,'dataSA'=>$dataSA,'imgSA'=>$imgSA, 'cantCuadrantes' => $cantCuadrantes, 'cantEstantes' => $cantEstantes);
					$this->load->view('storageUpdate/parametrizerU',$data);
				}
			}
		}
		elseif ($tipo == 4) 
		{
			$pruebaIdSA=$this->input->post('pruebaIdSA');// capturo el valor del subalmacen
			if (empty($pruebaIdSA)) //pregunto si la variable tiene algun dato (existe)
			{// no existe o no tiene nada esa variable cargo la vista de certificacion de parametros
				$this->load->view('storage/certificacionParametros');
			}
			else
			{//si existe cambio la vista por la vista de edicion de parametrizacion de almacenes y le paso por un arreglo los  valores que se encuentran almacenados en la base de datos para ese subalmacen.
				$pruebaIdSA=$this->input->post('pruebaIdSA');
				$almacen=$this->input->post('almacen');
				$datosAlmacen=$this->storage_model->selectCertifica($pruebaIdSA);
				$cantCuadrantes=$this->storageUpdate_model->cuadrantesExistentes($pruebaIdSA);
				$cantEstantes=$this->storageUpdate_model->estantesExistentes($pruebaIdSA);
				$datosCuadrantes=$this->storage_model->certificacionCuadrantes($pruebaIdSA);
				$datosEstantes=$this->storage_model->certificacionEstantes($pruebaIdSA);
				$datosRack=$this->storage_model->certificacionRack($pruebaIdSA);
				$imgSA=$this->storageUpdate_model->imgSA($pruebaIdSA);
				$data = array('datosAlmacen' => $datosAlmacen,'datosCuadrantes'=>$datosCuadrantes,'datosEstantes'=>$datosEstantes,'datosRack'=>$datosRack,'pruebaIdSA'=>$pruebaIdSA,'almacen',$almacen,'imgSA'=>$imgSA,'cantCuadrantes'=>$cantCuadrantes,'cantEstantes'=>$cantEstantes);
				$this->load->view('storage/certificacionParametros',$data);
			}
		}
		
	}

	/**
 	* la funcion load view sirve para cargar vista para la creacion del almacen, recibe como parametro $tipo
 	*tipos:
 	*1) Crear almacen
 	*/

	public function loadView($tipo)
	{
		if ($tipo == 1) 
		{
			$this->load->view('modal/createStorage');
		}
		elseif ($tipo == 2)
		{
			$id_subalmacen=$_POST['id_subalmacen'];
			$data=$this->storage_model->loadsubalmacen($id_subalmacen);
			$this->load->view('modal/updateStorage',$data);
		}
		elseif ($tipo == 3)
		{
			$id_subalmacen=$_POST['id_subalmacen'];
			$data=$this->storage_model->loadsubalmacen($id_subalmacen);
			$this->load->view('modal/viewStorage',$data);
		}
		elseif ($tipo == 4)
		{
			$almacen=$this->input->post('almacen');
			$nombre=$this->input->post('nombre');
			$data= array('almacen' => $almacen,'nombre' => $nombre);
			$this->load->view('inicio',$data);
		}
	}

	/**
	 * la funcion confi recibe como parametro $tipo que indica que tipo de vista va a desplegar al momento que se esta *realizando la parametrizacion de almacenes.
	 * tipos:
	 *1) Configuracion de Cuadrantes.
	 *2) Configuracion de Estantes.
	 *3) Configuracion de Rack
	 */
	public function confi($tipo)
	{
		if ($tipo == 1) 
		{
			$this->load->view('storage/confiCuadrante');
		}
		elseif ($tipo == 2) 
		{
			$this->load->view('storage/confiEstante');
		}
		elseif ($tipo == 3) 
		{
			$this->load->view('storage/confiRack');
		}
		
	}

	/**
	 * la funcion newStorage gestiona la creacion de nuevos almacenes recibe como parametro el id del usuario
	 */

	public function newStorage($idu)
	{
		$nombre_almacen_new=$this->input->post('nombre_almacen_new');
		$data=$this->storage_model->newStorage($nombre_almacen_new,$idu);
		echo json_encode($data);
	}

	/**
	 * la funcion newStorage gestiona la creacion de nuevos sub almacenes recibe como parametro el id del usuario
	 */
	public function Subalmacen($idu)
	{
		$nombre_subalmacen=$this->input->post('nombre_subalmacen');
		$id_almacen_new=$this->input->post('id_almacen_new');
		$datas=$this->storage_model->Subalmacen($nombre_subalmacen,$idu,$id_almacen_new);
		$data=str_replace('[[""]]', '', $datas);
		echo json_encode($data);
	}

	/**
	 * la funcion insertCuadrante recibe los parametros del formulario de creacion de cuadrantes y gestiona la insercion de los datos en el modelo storage_model en el metodo insertCuadrante, captura por post los parametros enviados por el ajax para su posterior envio al modelo.
	 si la insercion da distinto  a 0 muestra la configuracion de paletas.
	 en caso contrario envia mediante json el valor devuelto de la insercion a la vista.
	 */
	public function insertCuadrante()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$cantCuadrante=$this->input->post('cantCuadrante');
		$insert=$this->storage_model->insertCuadrante($pruebaIdSA,$cantCuadrante);
		$data= array('insert' => $insert, 'cantCuadrante' => $cantCuadrante );
		if ($insert != 0) 
		{
			$this->load->view('storage/confiPaleta',$data);
		}
		else
		{
			echo json_encode($insert);
		}
		
	}

	/**
	 * la funcion IConfiPaleta gestiona la insercion de la configuracion de paletas captura mediante post por ajax. y luego lo envia al modelo storage_model en el metodo IConfiPaleta para su insercion.
	 */

	public function IConfiPaleta()
	{
		$id_cuadrante=$this->input->post('id_cuadrante');
		$paleta=$this->input->post('paleta');
		$data=$this->storage_model->IConfiPaleta($id_cuadrante,$paleta);
	}

	/**
	 * la funcion IConfiEntrepano recibe los parametros del formulario de creacion de estantes y gestiona la insercion de los datos en el modelo storage_model en el metodo insertEstante, captura por post los parametros enviados por el ajax para su posterior envio al modelo.
	 si la insercion da distinto  a 0 muestra la configuracion de entrepaños.
	 en caso contrario envia mediante json el valor devuelto de la insercion a la vista.
	 */

	public function insertEstante()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$cantEstante=$this->input->post('cantEstante');
		$insert=$this->storage_model->insertEstante($pruebaIdSA,$cantEstante);
		$data= array('insert' => $insert, 'cantEstante' => $cantEstante );
		if ($insert != 0) 
		{
			$this->load->view('storage/confiEntrepano',$data);
		}
		else
		{
			echo json_encode($insert);
		}
	}

	/**
	 * la funcion IConfiEntrepano gestiona la insercion de la configuracion de entrepaño, captura mediante post por ajax. y luego lo envia al modelo storage_model en el metodo IConfiEntrepano para su insercion.
	 */

	public function IConfiEntrepano()
	{
		$id_estante=$this->input->post('id_estante');
		$entrepano=$this->input->post('entrepano');
		$data=$this->storage_model->IConfiEntrepano($id_estante,$entrepano);
	}

	/**
	 * la funcion insertRack recibe los parametros del formulario de creacion de rack y gestiona la insercion de los datos en el modelo storage_model en el metodo insertRack, captura por post los parametros enviados por el ajax para su posterior envio al modelo.
	 si la insercion da distinto  a 0 muestra la configuracion de rack.
	 en caso contrario envia mediante json el valor devuelto de la insercion a la vista.
	 */

	public function insertRack()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$name_rack=$this->input->post('name_rack');
		$insert=$this->storage_model->insertRack($pruebaIdSA,$name_rack);
		$data= array('insert' => $insert, 'name_rack' => $name_rack );
		if ($insert != 0) 
		{
			$this->load->view('storage/rackInfo',$data);
		}
		else
		{
			echo json_encode($insert);
		}
	}

	/**
	 * la funcion IConfiRack gestiona la insercion de la configuracion de filas y columnas, captura mediante post por ajax. y luego lo envia al modelo storage_model en el metodo adicionalRack para su insercion.
	 */

	public function IConfiRack()
	{
		$id_rack=$this->input->post('id_rack');
		$fRack=$this->input->post('fRack');
		$cRack=$this->input->post('cRack');
		$insert=$this->storage_model->IConfiRack($id_rack,$fRack,$cRack);
		$name_rack=$this->storage_model->dataRack($id_rack);
		$espacios=$this->storage_model->adicionalRack($id_rack);
		$data=array('name_rack'=>$name_rack,'espacios'=>$espacios);
		$this->load->view('tables/inforackcreado',$data);
	}

	/**
	 * la funcion SubalmacenU gestiona la actualizacion de un subalmacen recibe por post los datos del formulario. y los envia al modelo storageUpdate_model en su metodo  SubalmacenU para luego la respuesta ser envia mediante un json a la vista.
	 */
	public function SubalmacenU()
	{
		$id_sa=$this->input->post('id_sa');
		$nombre_subalmacen=$this->input->post('nombre_subalmacen');
		$id_almacen_new=$this->input->post('id_almacen_new');
		$data=$this->storageUpdate_model->SubalmacenU($nombre_subalmacen,$id_sa,$id_almacen_new);
		echo json_encode($data);
	}

	/**
	 * la funcion cuadranteE gestiona el eliminar de una paleta del cuadrante recibe por post el id de la paleta a eliminar. y los envia al modelo storageUpdate_model al metodo  deleteCER para luego la respuesta ser envia mediante un json a la vista.
	 */
	public function cuadranteE()
	{
		$id=$this->input->post('id');
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$idcer=$this->input->post('idcer');
		$deleteCuadrante=$this->storageUpdate_model->deleteCER($id,$pruebaIdSA,$idcer);
		echo json_encode($deleteCuadrante);
	}

	/**
	 * la funcion cuadranteSE gestiona el eliminar del cuadrante recibe por post el id del cuadrante a eliminar. y los envia al modelo storageUpdate_model al metodo  deleteCER para luego la respuesta ser envia mediante un json a la vista.
	 */

	public function cuadranteSE()
	{
		$id=$this->input->post('id');
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$idcer=$this->input->post('idcer');
		$deleteCuadrante=$this->storageUpdate_model->deleteSCER($id,$pruebaIdSA,$idcer);
		echo json_encode($deleteCuadrante);
	}

	/**
	 * la funcion estanteE gestiona el eliminar de un entrepaño del estante recibe por post el id de la entrepaño a eliminar. y los envia al modelo storageUpdate_model al metodo  deleteSCER para luego la respuesta ser envia mediante un json a la vista.
	 */

	public function estanteE()
	{
		$id=$this->input->post('id');
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$idcer=$this->input->post('idcer');
		$deleteEstante=$this->storageUpdate_model->deleteCER($id,$pruebaIdSA,$idcer);
		echo json_encode($deleteEstante);
	}

	/**
	 * la funcion estanteSE gestiona el eliminar del estante recibe por post el id del estante a eliminar. y los envia al modelo storageUpdate_model al metodo  deleteSCER para luego la respuesta ser envia mediante un json a la vista.
	 */

	public function estanteSE()
	{
		$id=$this->input->post('id');
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$idcer=$this->input->post('idcer');
		$deleteEstante=$this->storageUpdate_model->deleteSCER($id,$pruebaIdSA,$idcer);
		echo json_encode($deleteEstante);
	}

	/**
	 * la funcion rackE gestiona el eliminar de un rack recibe por post el id del rack a eliminar. y los envia al modelo storageUpdate_model al metodo  deleteCER para luego la respuesta ser envia mediante un json a la vista.
	 */

	public function rackE()
	{
		$id=$this->input->post('id');
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$idcer=$this->input->post('idcer');
		$deleteRack=$this->storageUpdate_model->deleteCER($id,$pruebaIdSA,$idcer);
		echo json_encode($deleteRack);
	}

	/**
	 * la funcion subalmacenE gestiona el eliminar de un subalmacen recibe por post el id del subalmacen a eliminar. y los envia al modelo storageUpdate_model al metodo  deleteSA para luego la respuesta ser envia mediante un json a la vista.
	 */

	public function subalmacenE()
	{
		$id=$this->input->post('id');
		$deleteSubalmacen=$this->storageUpdate_model->deleteSA($id);
		echo json_encode($deleteSubalmacen);
	}

	/**
	 * la funcion search se usa para cargar toda la informacion relacionada al subalmacen que se haya elegido, recibiendo por post  el id del subalmacen y el id del almacen para pasarlo por parametro en cada una de alas consultas al modelo.
	 */
 
	public function search()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$almacen=$this->input->post('almacen');
		$datosAlmacen=$this->storage_model->selectCertifica($pruebaIdSA);
		$cantCuadrantes=$this->storageUpdate_model->cuadrantesExistentes($pruebaIdSA);
		$cantEstantes=$this->storageUpdate_model->estantesExistentes($pruebaIdSA);
		$datosCuadrantes=$this->storage_model->certificacionCuadrantes($pruebaIdSA);
		$datosEstantes=$this->storage_model->certificacionEstantes($pruebaIdSA);
		$datosRack=$this->storage_model->certificacionRack($pruebaIdSA);
		$imgSA=$this->storageUpdate_model->imgSA($pruebaIdSA);
		$data = array('datosAlmacen' => $datosAlmacen,'datosCuadrantes'=>$datosCuadrantes,'datosEstantes'=>$datosEstantes,'datosRack'=>$datosRack,'pruebaIdSA'=>$pruebaIdSA,'almacen',$almacen,'imgSA'=>$imgSA,'cantCuadrantes' =>$cantCuadrantes,'cantEstantes' =>$cantEstantes);
		$this->load->view('modal/certificacionParametros',$data);
	}

	/**
	 * la funcion edit se usa para cargar el menu que se mostrara en el modal de editar subalmacen recibiendo por post el id del subalmacen y del almacen para enviarlo como parametro al modelo.
	 */

	public function edit()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$almacen=$this->input->post('almacen');
		$nombre=$this->storageUpdate_model->infoSA($pruebaIdSA);
		$data= array('pruebaIdSA' => $pruebaIdSA,'almacen' => $almacen,'nombre'=>$nombre);
		$this->load->view("storageUpdate/inicio",$data);
	}

	/**
	 * la funcion searchStorage se usa para cargar toda la informacion relacionada al almacen que se haya elegido
	 */
 
	public function searchStorage()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');

		/*$datosAlmacen=$this->storage_model->selectCertifica($pruebaIdSA);
		$cantCuadrantes=$this->storageUpdate_model->cuadrantesExistentes($pruebaIdSA);
		$cantEstantes=$this->storageUpdate_model->estantesExistentes($pruebaIdSA);
		$datosCuadrantes=$this->storage_model->certificacionCuadrantes($pruebaIdSA);
		$datosEstantes=$this->storage_model->certificacionEstantes($pruebaIdSA);
		$datosRack=$this->storage_model->certificacionRack($pruebaIdSA);
		$imgSA=$this->storageUpdate_model->imgSA($pruebaIdSA);
		$data = array('datosAlmacen' => $datosAlmacen,'datosCuadrantes'=>$datosCuadrantes,'datosEstantes'=>$datosEstantes,'datosRack'=>$datosRack,'pruebaIdSA'=>$pruebaIdSA,'almacen',$almacen,'imgSA'=>$imgSA,'cantCuadrantes' =>$cantCuadrantes,'cantEstantes' =>$cantEstantes);
		$this->load->view('modal/certificacionParametros',$data);*/
		echo "si";
	}

	/**
	 * la funcion editStorage se usa para cargar el menu que se mostrara en el modal de editar almacen recibiendo por post el id del almacen
	 */

	public function editStorage()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$almacen=$this->input->post('almacen');
		$nombre=$this->storageUpdate_model->infoSA($pruebaIdSA);
		$data= array('pruebaIdSA' => $pruebaIdSA,'almacen' => $almacen,'nombre'=>$nombre);
		$this->load->view("storageUpdate/inicio",$data);
	}

	/**
	 * la funcion viewEdit que recibe como parametro $tipo es quien indica que vista se va a amostrar al momento de realizar la edicion de la parametrizacion del subalmacen
	 tipo:
	 1) Editar cuadrante
	 2) Editar Paleta
	 3) Editar Estante
	 4) Editar Entrepaño
	 5) Editar Rack
	 6) Editar Fila Rack
	 7) Editar Columna Rack
	 */

	public function viewEdit($tipo)
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');// capturo el id del subalmacen enviado por post desde ajax
		//valido que tipo es
		if ($tipo == 1) 
		{//si es 1 consulto al modelo el ultimo cuadrante que hay creado
			$datos=$this->storageUpdate_model->selectCuadrante($pruebaIdSA);
		}
		elseif ($tipo == 2) 
		{//si es 2 consulto al modelo todos cuadrantes que estan creados
			$datos=$this->storageUpdate_model->selectIdCuadrante($pruebaIdSA);
		}
		elseif ($tipo==3) 
		{//si es 3 consulto al modelo el ultimo estante que hay creado
			$datos=$this->storageUpdate_model->selectEstante($pruebaIdSA);
		}
		elseif ($tipo==4) 
		{//si es 4 consulto al modelo todos estantes que estan creados
			$datos=$this->storageUpdate_model->selectIdEstante($pruebaIdSA);
		}
		elseif ($tipo==5) 
		{//si es 5 consulto al modelo el ultimo rack que hay creado
			$datos=$this->storageUpdate_model->selectRack($pruebaIdSA);
		}
		elseif ($tipo==6) 
		{//si es 6 consulto al modelo todos racks que estan creados 
			$datos=$this->storageUpdate_model->selectIdRack($pruebaIdSA);
		}
		elseif ($tipo==7) 
		{//si es 7 consulto al modelo todos racks que estan creados
			$datos=$this->storageUpdate_model->selectIdRack($pruebaIdSA);
		}
		
		$data = array('datos' => $datos );

		if ($tipo == 1) 
		{
			$this->load->view('storage/confiCuadrante',$data);
		}
		elseif ($tipo==2) 
		{
			$this->load->view('storageUpdate/paletaUpdate',$data);
		}
		elseif ($tipo==3)
		{
			$this->load->view('storage/confiEstante',$data);
		}
		elseif ($tipo==4)
		{
			$this->load->view('storageUpdate/entrepanoUpdate',$data);
		}
		elseif ($tipo==5)
		{
			$this->load->view('storageUpdate/confiRackU',$data);
		}
		elseif ($tipo==6)
		{
			$this->load->view('storageUpdate/newRowRack',$data);
		}
		elseif ($tipo==7)
		{
			$this->load->view('storageUpdate/newColumnRack',$data);
		}
		else
		{
			$this->load->view('errors/cli/error_404');
		}
	}

	/**
	 * la funcion insertCuadranteU gestiona el envio de datos cargados en la vista para actualizar en base de datos los cuadrantes.
	 */

	public function insertCuadranteU()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');// capturo el id del subalmacen enviado por ajax
		$datos=$this->storageUpdate_model->selectCuadrante($pruebaIdSA);// consulto al modelo pasando por parametro el id del subalmacen
		$cantCuadrante=$this->input->post('cantCuadrante');// capturo la cantidad de cuadrantes a agregar al subalmacen
		$insert=$this->storageUpdate_model->insertCuadranteU($pruebaIdSA,$cantCuadrante); // envio a el modelo la cantidad de cuadrantes a sumarle al subalmacen escogido
		$data= array('insert' => $insert, 'cantCuadrante' => $cantCuadrante,'datos' => $datos );
		if ($insert != 0) //pregunto si el resultado de lo que devuelve la consulta al modelo es distinto a 0
		{// si se cumple la condicion me carga la vista para insertar la cantidad de paletas que tiene el cuadrante antes actualizado
			$this->load->view('storage/confiPaleta',$data);
		}
		else
		{// si no envio al ajax el resultado de insert
			echo json_encode($insert);
		}
		
	}

	/**
	 * la funcion detalles me consulta el nombre del cuadrante escogido
	 */

	public function detalles()
	{
		$id=$this->input->post('id');
		$data=$this->storageUpdate_model->detalles($id);
		echo json_encode($data);
	}

	/**
	 * la funcion detallesPaleta sirve para consultar y devolver la ultima paleta que fue cargada en el cuadrante elegido.
	 */

	public function detallesPaleta()
	{
		$id=$this->input->post('id');
		$data=$this->storageUpdate_model->load_name($id);
		echo json_encode($data);
	}

	/**
	 * la funcion IConfiPaletaU sirve para insertar los valores cargado para las paletas nuevas, o sea es la actualizacion de un cuadrante cuando se le agragan mas paletas.
	 */

	public function IConfiPaletaU()
	{
		$id=$this->input->post('id');
		$paleta=$this->input->post('paleta');
		$data=$this->storageUpdate_model->IConfiPaletaU($id,$paleta);
	}

	/**
	 * la funcion insertEstanteU gestiona el envio de datos cargados en la vista para actualizar en base de datos los estantes.
	 */

	public function insertEstanteU()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');// capturo el id del subalmacen enviado por ajax
		$datos=$this->storageUpdate_model->selectEstante($pruebaIdSA);// consulto al modelo pasando por parametro el id del subalmacen
		$cantEstante=$this->input->post('cantEstante');// capturo la cantidad de estantes a agregar al subalmacen
		$insert=$this->storageUpdate_model->insertEstanteU($pruebaIdSA,$cantEstante);// envio a el modelo la cantidad de estantes a sumarle al subalmacen escogido
		$data= array('insert' => $insert, 'cantEstante' => $cantEstante,'datos' => $datos );
		if ($insert != 0) //pregunto si el resultado de lo que devuelve la consulta al modelo es distinto a 0
		{// si se cumple la condicion me carga la vista para insertar la cantidad de estantes que tiene el cuadrante antes actualizado
			$this->load->view('storage/confiEntrepano',$data);
		}
		else
		{// si no envio al ajax el resultado de insert
			echo json_encode($insert);
		}
		
	}

	/**
	 * la funcion detallesE me consulta el nombre del estante escogido
	 */
	public function detallesE()
	{
		$id=$this->input->post('id');
		$data=$this->storageUpdate_model->detallesE($id);
		echo json_encode($data);
	}


	/**
	 * la funcion detallesEntrepanoE sirve para consultar y devolver la ultimo entrepano que fue cargado en el esatnte elegido.
	 */
	public function detallesEntrepanoE()
	{
		$id=$this->input->post('id');
		$data=$this->storageUpdate_model->load_nameE($id);
		echo json_encode($data);
	}
	/**
	 * la funcion IConfiEntrepanoU sirve para insertar los valores cargado para los entrepaños nuevos, o sea es la actualizacion de un estante cuando se le agragan mas entrepaños.
	 */
	public function IConfiEntrepanoU()
	{
		$id=$this->input->post('id');
		$entrepano=$this->input->post('entrepano');
		$data=$this->storageUpdate_model->IConfiEntrepanoU($id,$entrepano);
	}

	/**
	 * la funcion insertRackU sirve para insertar el nuevo nombre cargado para un racks, o sea es la actualizacion del nombre del rack.
	 */
	public function insertRackU()
	{
		$pruebaIdSA=$this->input->post('pruebaIdSA');
		$name_rack=$this->input->post('name_rack');
		$insert=$this->storage_model->insertRack($pruebaIdSA,$name_rack);
		$data= array('insert' => $insert, 'name_rack' => $name_rack );
		if ($insert != 0) //pregunto si el resultado de lo que devuelve la consulta al modelo es distinto a 0
		{// si se cumple la condicion me carga la vista para insertar la cantidad de filas y columnas que tiene el rack actualizado
			$this->load->view('storageUpdate/rackInfoU',$data);
		}
		else
		{// si no envio al ajax el resultado de insert
			echo json_encode($insert);
		}
	}

	/**
	 * la funcion detallesRowR me consulta el nombre de la ultima fila agregada al rack escogido
	 */
	public function detallesRowR()
	{
		$id_rack=$this->input->post('id_rack');
		$data=$this->storageUpdate_model->load_nameR($id_rack);
		echo json_encode($data);
	}

	/**
	 * la funcion IConfiRackU sirve para insertar el nuevo numero de filas con las columnas cargadas por el usuario
	 */
	public function IConfiRackU()
	{
		$id_rack=$this->input->post('id_rack');// capturo el valor del id del rack que viene del ajax
		$fRack=$this->input->post('fRack');// capturo el valor del numero de filas que viene del ajax
		$cRack=$this->input->post('cRack');// capturo el valor de columas que viene del ajax
		$fRackh=$this->input->post('fRackh');// capturo el valor del ultimo valor de la fila que viene del ajax
		$insert=$this->storageUpdate_model->IConfiRackU($id_rack,$fRack,$cRack,$fRackh);
		echo json_encode($insert);
	}

	/**
	 * la funcion detallesColumnR me consulta el nombre de la ultima columna agregada a una fila del rack escogido
	 */
	public function detallesColumnR()
	{
		$id_rack=$this->input->post('id_rack');
		$data=$this->storageUpdate_model->load_nameCR($id_rack);
		echo json_encode($data);
	}

	/**
	 * la funcion IConfiRackU sirve para insertar el nuevo numero de columnas a una fila cargadas por el usuario
	 */
	public function IConfiRackUI()
	{
		$id_rack=$this->input->post('id_rack');// capturo el valor del id del rack que viene del ajax
		$id_fila=$this->input->post('id_fila');// capturo el valor del id de la fila que viene del ajax
		$cRack=$this->input->post('cRack');// capturo el valor de columas que viene del ajax
		$cRackh=$this->input->post('cRackh');// capturo el valor del ultimo valor de la coumna que viene del ajax
		$insert=$this->storageUpdate_model->IConfiRackUI($id_rack,$id_fila,$cRack,$cRackh);
		echo json_encode($insert);
	}
	
	/**
	 * la funcion detallesColumnR me consulta el vañlor de la ultima columna agregada a una fila del rack escogido
	 */
	public function detalleColumn()
	{
		$id_rack=$this->input->post('id_rack');
		$id_fila=$this->input->post('id_fila');
		$data=$this->storageUpdate_model->loadColumn($id_rack,$id_fila);
		echo json_encode($data);
	}

}
?>