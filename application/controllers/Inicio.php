<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	function __construct()
	{
		parent::__construct(); 
		$this->load->model('storage_model', '', TRUE);//cargo el modelo para usarlo mas adelante en las consultas
		$this->load->helper(array('url','form'));//cargo los helpers
	}

	/**
	 * la funcion index se ejecuta cuando dentro del cocai-ivss se clickquea en la opcion parametrizacion de almacenes
	 * y se carga a traves de una consulta ajax, recibo como parametro el id del cliente que esta logueado.
	 */

	public function index($idu)
	{
		$datosAlmacen=$this->storage_model->select($idu);// se consulta en el modelo (STORAGE_MODEL) en el metodo "select" y paso por parametros el id del usuario.
		$data = array('datosAlmacen' => $datosAlmacen,'idu'=>$idu);// almaceno un arreglo clave-valor lo obtenido de la consulta anterior para pasarla a la vista.
		$this->load->view('storage/storage',$data);//cargo la vista pasandole $data que contiene la información consultada.
	}


	/**
	 * la funcion loadStorage, carga los almacenes que estan asociados a el usuario. recibe como parametro el id del usuario
	 *esta funcion se ejecuta a traves de ajax.
	 */
	public function loadStorage($idu)
	{
		$almacen=$this->storage_model->almacen($idu);//se consulta el modelo (STORAGE_MODEL) en el metodo almacen, pasando como parametro el id del usuario
		$data = array('almacen' => $almacen );
		$this->load->view('tables/loadstorage',$data);
	}

	/**
	 * la funcion loadSubStorage, carga los sub almacenes que se encuantran asociados a un almacen en especifico.
	 *esta funcion se ejecuta a traves de ajax.
	 */
	public function loadSubStorage()
	{
		$almacen=$this->input->post('almacen');//almaceno en la variable $almacen el valor pasado por post del ajax
		$subalmacen=$this->storage_model->loadSubStorage($almacen);//se consulta el modelo (STORAGE_MODEL) en el metodo loadSubStorage pasando por parametro el id del almacen
		$info=$this->storage_model->infoStorage($almacen);//se consulta el modelo (STORAGE_MODEL) en el metodo infoStorage pasando por parametro el id del almacen
		$data= array('subalmacen' => $subalmacen , 'info' => $info );// almaceno un arreglo clave-valor lo obtenido de la consulta anterior para pasarla a la vista.
		$this->load->view('tables/loadsubstorage',$data);// almaceno un arreglo clave-valor lo obtenido de la consulta anterior para pasarla a la vista.
	}

}
